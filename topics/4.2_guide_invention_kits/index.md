---
layout: default
title: 4 Cardboard Invention Kits Facilitator Guide
nav_order: 12
mathjax: true
---


# Faciltator Guide: Fabricate a Cardboard Invention Kit
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Learners will use design and laser cut their very own two-dimensional invention kits that can slot together to make three-dimensional structures.

Finished kits can be used for brainstorming, rapid prototyping, and more.

This activity introduces the basics of press-fit assembly, (including using calipers to measure stock material), 2D design in xTool's XCS software, and the laser cutter.


### Context

In the real world, engineers and inventors often face tight deadlines for solving problems. It’s easy to get overwhelmed—or to not know where to start!

Invention kits can help facilitate brainstorming and accelerate the prototyping process. They provide a ready-made set of components that can be easily assembled and reconfigured to test out different design concepts or build scale models.

LEGO bricks are a great example of an invention kit. They can be quickly assembled or disassembled in a variety of ways to illustrate a concept or test out an idea. These invention kits will work similarly, allowing learners to build upward or outward—using pieces they design themselves!

{:toc}



## Scalability
### 1. Exposure: Geometric Invention Kits
Design and laser cut simple geometric shapes that can be assembled into three-dimensional forms.

- Uses 3D-printed joints (supplied, with customizable angles), meaning learners don’t have to add slots for press-fit construction.
- Introduces, defines, and teaches how to determine a tool's *kerf*.
- **Curricular tie-ins**: math, geometry
  - Design a kit that contains X number of symmetrical vs. asymmetrical shapes, or can be used to build specific three-dimensional geometries (isosidododecahedron, etc).


### 2. Exploration: Cardboard Critters
Design and laser cut biologically-inspired shapes that can be used to built wild and wacky custom critters.

- Incorporates laser-cut slots into the design process to teach press-fit construction.
  - Shapes can be drawn freehand and then cleaned up digitally: antennae, legs, etc.
- Introduces, defines, and teaches how to:
  - determine a tool's kerf;
  - use calipers to precisely measure a material's thickness;
  - calculate (and test to confirm) optimal slot size for press-fit assemblies.
- **Curricular tie-ins**: story starters (ELA and/or biology)
  - Construct a critter and then write a story about its origin/evolution.  (Can optionally have learners swap critters so they are writing about one they didn't design themselves.)

### 3. Deep Dive: Scale Models
Use digital design, fabrication, and press-fit assembly to create a functional object of your own design or replicate a real-world one.

- Invites much more complexity and precision, with most sessions focused on the design process and assembly (relatively fewer on the fabrication itself).
- Introduces, defines, and teaches how to:
  - determine a tool's kerf;
  - use calipers to precisely measure a material's thickness;
  - calculate (and test to confirm) optimal slot size for press-fit assemblies.
  - Can optionally incorporate 3D-printed "accessories" to liven up designs.
- **Curricular tie-ins**: math (measurements & ratios), history, civil engineering, engineering, product design, etc.
  - Design a press-fit chess kit.
  - Create a scale model of (or invent your own) historical monuments.
  - Engineer a city building or feature (class-wide).
  - Model a vehicle.

### Extension: Actuated Assemblies
Add additional holes to fit dowels, chopsticks, pipe cleaners, LEDs, or servo motors in the vein of John Umekubo to expand a kit’s creative potential.

  - Simple servos can be taped or press-fit into place and programmed to help assemblies walk, flap, turn, spin, roll, and more!
  - LEDs can add some light to a project, (for example, to give the illusion of a city building lit from within!).



## Materials
- Cardboard - 1 sheet per learner
  - Note: this should be corrugated cardboard that compresses easily but doesn't crease easily.
- (Optional) Decorative elements:
  - Pipe cleaners
  - Brads
  - Chopsticks or dowel rods



{:toc}


{:toc}


## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas:
    - Defining and Delimiting Engineering Problems (3-5-ETS1.A),
    - Developing Possible Solutions ((3-5-ETS1.B),
    - Optimizing the Design Solution (3-5-ETS1.C)
  - Science and Engineering Practice:
    - Planning and Carrying Out Investigations (3-5-ETS1-2);

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Demonstrate an understanding of two-dimensional design principles.
2. Apply spatial reasoning skills to visualize how two-dimensional shapes can be assembled into three-dimensional structures.
3. Consider mathematical concepts such as symmetry and proportion in design.
4. Calculate a kerf.
5. Utilize their invention kits for rapid prototyping, real-world problem-solving, and creative exploration in various contexts.
6. Critically evaluate and iterate upon their designs based on feedback and testing.
7. (Optional) Experiment with different materials and their properties to optimize the design and functionality of their invention kits.



### Fabrication goals
Learners will be able to…
1. Operate the laser cutter safely and effectively, following proper procedures.
2. Identify and select appropriate materials for laser cutting based on project requirements.
3. Use calipers to measure the thickness of stock material.
4. Determine the optimal slot width for press-fit assemblies.
5. Define and account for kerf.
6. Understand and use parametric design principles.
7. (Optional) Incorporate champfering into their designs when appropriate.
s to help direct and reinforce creases.



## Facilitator Notes
### 1. Setup & pre-preparation
- This activity works best with high-quality corrugated cardboard because it is compressible and has some give to help with assembly. It can be replicated with other materials (wood, craft foam, cardboard, acrylic), but pieces may be harder to assemble and take apart.
- Short on time?
  - Pre-design or pre-cut test pieces for learners to use to determine the optimal slot width for their designs.
  - Consider determining optimal laser cutter settings for your material of choice prior to running this activity with your learners.

### 2. Material considerations
- This activity works best with high-quality corrugated cardboard because it is compressible and has some give to help with assembly. It can be replicated with other materials (wood, craft foam, cardboard, acrylic), but pieces will be harder to assemble and take apart.
- Ask learners to limit their designs to one sheet of material.

### 3. Time management tips
- Adding 1-2 per object tabs allows you to remove an entire piece of cardboard from the printing bed between jobs.
  - Learners can then finish “punching out’ their pieces back at their stations.

### 4. Brainstorming
- Pose prompts to overcome “blank page syndrome” and encourage creative thinking.
  - Build a character, building, or prop and write a story that incorporates it (ELA).
  - Re-create a favorite landmark or design a new monument (civics/history).
  - Design something with multiple degrees of symmetry (math/geometry).
  - Create something biomimetic/biologically-inspired (biology).
  - Design and categorize multiple types of pieces:
    - anatomical = heads, arms, legs, hands, feet, tails
    - whimsical/decorative = wings, horns, accessories
    - mechanical = wheels, gears
    - structural = beams

- Once designed or cut, invite learners to discuss/trade pieces of their designs to inspire further creativity.

### 5. Design considerations to share with learners
- Consider your material constraints (in this case, the size of your sheet of cardboard) while designing.
- Optimize your layout to minimize material waste: place your components as close together as possible.  
  - You can flip and rotate these to minimize margins!
- Multiples of the same shape can be helpful! Use the patterning tools in your design software to quickly replicate objects without copying & pasting.
- Consider creating connectors at different angles and with different numbers of "arms".
- If time allows, you can add decorative detailing like engravings or scored lines.
  - Be sure to format these in XCS so they process as the correct operation.
- Consider using (pieces of) your scrap cardboard as a design element when building as well!
- For advanced learners: incorporate flexures and joints.
- If time allows:
  - Introduce champfers and compare assembly with champfered vs. unchampfered pieces.
  - Cut the same design on wood and ask learners to compare assembly with a rigid vs. compressible material.


## Discussion questions
- **Math**
1. Can you write a formula for calculating the kerf of a material? Why is kerf important in laser cutting?
2. What mathematical concepts are involved in designing press-fit and slot-and-tab assemblies?
3. How can understanding symmetry help in designing geometrically inspired kits?
4. What factors do you need to identify in order to create the pieces for a three-dimensional assembly, like a cube or a dodecahedron?

- **Art & Design**
1. What are the key considerations when designing shapes for press-fit assemblies?
2. Why do you think we used cardboard for this activity? How does the choice of material affect the design and functionality of your construction kit?
3. How can you ensure that your design is both functional and aesthetically pleasing?
4. What challenges did you encounter in the design process, and how did you overcome them?
5. (Optional, if relevant) How can parametric design principles improve the efficiency and adaptability of your designs?

- **Science: Form & Movement**
1. What basic principles of physics (pushes and pulls) are involved in making your pop-up work?
2. How do mountain and valley folds contribute to the movement and structure of pop-up elements?
3. Can you think of other real-world applications where similar folding mechanisms are used?

- **English Language Arts**
1. How can you use descriptive language to enhance the story about your critter or invention?
2. What are some creative ways to incorporate your constructed model into a written piece?
3. How can designing and building a prop help in visual storytelling?

- **Engineering & Technology**
1. What safety procedures should be followed when operating a laser cutter?
2. How can different laser cutter settings (power, speed, passes) affect the cutting outcome?
3. What is the importance of iterative testing in the design and fabrication process?
4. How can you incorporate other materials or tools to enhance your invention kits?

- **Creativity & Problem Solving**
1. How did you approach the design process for your invention kit?
2. What creative techniques did you use to make your design unique?
3. How did you solve any problems that arose during the cutting and assembly process?
4. What improvements or modifications would you make to your design based on feedback and testing?
5. How can you use your invention kit for brainstorming and rapid prototyping?
