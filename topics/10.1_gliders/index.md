---
layout: default
title: 10. Balsa Gliders Activity Steps
nav_order: 24
mathjax: true
---


# Winging It: Engineering on the Fly
{: .no_toc}
By Claire Dorsett

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview
Design and fabricate a three-piece glider!

Laser cut a fuselage, vinyl cut cardstock wings and tail, and 3D print clip-on weights.

Then test your design, tweak it, and try again!

### Context
Four forces act upon a plane in the air: thrust, lift, gravity, and drag.

  1. **Thrust**: the force propelling a plane forward. Thrust is most often generated by fuel combustion (or your throw!).
  2. **Lift**: the force acting upward on a plane’s wings to keep it aloft. Lift is generated as a plane moves forward and air moves more quickly across the top of its wings than beneath them. Generally, the larger the wings, the greater the lift.
  3. **Gravity**: the force pulling a plane’s mass down toward the ground. Lighter materials can help a plane stay airborne longer.
  4. **Drag**: the frictional force a plane experiences from air flowing past it. Drag is the opposite of thrust, and slows a plane down. It is largely influenced by its tail design.

Planes’ designs must balance all of these forces. The shapes, sizes, and materials used for the wings, body, nose, and tail can all drastically affect the way a plane flies. How can we engineer a glider to take advantage of these forces?

#### In the Real World:
Engineers don’t create perfect designs on the first try.

They test, redesign, and retest their ideas again and again, making small changes each time based on their observations. This is called iterative prototyping, and it’s a key part of the engineering design process. Instead of thinking of lackluster designs as “failures,” think of them as rough drafts of a finished product. Each one is a step closer to success.

#### Parametric Design
Parametric design is a smart way to tweak digital designs.

Instead of creating a new CAD model from scratch each time they need to make a change, engineers use something called parametric modeling. This means they assign adjustable variables to each dimension of their design instead of values.

These labels can be single variables, like x  in a math equation, or they can be a function of a variable, like x+6 or x/2. We could even make them a function of each other: wing width  could equal body length - tail width.  

Parametric design allows engineers to scale an entire model up or down or make small tweaks to a single part.


{:toc}

## Materials
- 1.5mm to 3mm balsa wood -or- 5mm corrigated cardboard
- Thick (~110lb) cardstock for vinyl cutter.
  - Note: construction paper is not advised, as it is too fibrous to cut cleanly.
- 3D printer filament (PLA) in a color of your choice
- 3D printer
- Laser cutter and/or vinyl cutter


## Activity Steps
Note: While the steps outlined below focus on a combination of laser and vinyl cutting, your glider's fuselage, wings, and tail can be cut entirely on either machine (entirely out of balsa for the laser cutter or cardstock for the vinyl cutter). The 3D printed weights are optional; paperclips can be subsituted when these aren't an option.

### 1. Ideate
It can help to first sketch the three main components of your glider, labeling the features of each.

#### 1.1 Choose your fuselage shape
1. Sketch your desired shape for your glider's body, or *fuselage*.
2. Label the characteristics you think will help your glider perform.
  - Consider: why have you chosen a pointed nose or tapered tail? How might these affect the performance of your glider?

#### 1.2 Choose your wing shapes
1. Sketch the desired shape of your glider's wings.
2. Again, label the characteristics you feel will help your wings achieve optimal lift or distance. The shape you choose will affect how far and fast your design can glide.
  - Is your glider a fast, accurate *dart* with sharply angled wings? Or a floating, meandering *glider* with large, tapered ones?

#### 1.3 Choose your tail shapee
1. Sketch your desired shape for your glider's tail.
2. Label the characteristics of your tail you feel will bolster your glider's performance.
  - Tails can provide significant steering and balance!

#### 1.4 Choose your slot placement
1. Consider where your wings and body will intersect. Draw some lines on your sketches to indicate this placement.
  - Where your wings are located can affect whether your design will nosedive or flounder. Aim for something nice and balanced.

### 2. Design
You can use any CAD, 2D, or 3D design software you like for these steps (2D is sufficient for the fuselage, wings, and tail; 3D is needed for the weights). While specifics will depend on which software you choose, the steps outlined below offer a high-level overview of the design process for your fuselage and wings.

#### 2.1 xDesign


### 3. Laser cut fuselage
Note: the same steps apply if you are also cutting the wings and tail on the laser cutter.

#### 3.1 Open in XCS
  1. Open xTool's Creative Studio software, (available for free download here).
  2. Start a new project.
  3. Import your designs (use the file icon or drag and drop)

#### 3.2 Configure for cutting
Unless you've added decorative design elements, we only need to configure for "cutting" here.
  1. Select your material type from the main menu. (We recommend using 1.5mm or 3mm balsa wood). If your material doesn't appear from the menu, select "user-defined material." You can manually enter your cut settings later.
    - If your original design was created in a separate software and is parametric, remember not to adjust its size here in XCS. If you resize outside of your CAD environment, the width of your slots (likely sized perfectly for your stock material) will also change.
  2. Select the design. In the object setting menu, select "cut" as your processing type. For 1/16” balsa wood, we suggest the following:
        - **Score**: 40/150/1
        - **Engrave** (raster): 30/200/1
        - **Cut**: 100/15/1

#### 3.3 Connect laser cutter:
  1. Turn on the laser cutter and connect it to the computer via USB.
  2. Click “connect device” and select the appropriate machine.
  3. Load stock material:
      - Open laser cutter lid and place stock onto honeycomb.
      - Manually drag laser head over center of stock.
      - Close lid.

#### 3.4 Auto-focus:
  1. Click auto-focus button (this looks like a crosshair symbol next to the distance box).
  2. Wait for machine to focus.
  3. Open lid.
  4. Manually drag laser head to desired position on stock. The red cross will act as a reference point, also appearing in the XCS software.

#### 3.5 Check framing:
  1. In XCS software, position your design files as desired relative to the red cross.
  2. Select “framing.” When instructed, press the button on the machine. (This can be done with the machine lid open - there is no laser running during framing.) The laser head will move to map the outer perimeter of the design’s processing area. If this does not fit on the stock or overlaps a previous cut, manually adjust the stock or digitally adjust the design’s position.

#### 3.6 Run the job:
  - Click “process” in XCS, followed by the button on the machine when instructed to do so.
  - **Important: Never leave the machine unattended while it is running!**

#### 3.7 Remove finished pieces:
  1. Gently check to make sure all pieces cut through. If they haven’t quite released, you can close the lid and run another cut pass. (Toggle the layer with the engraved design to “ignore” first.)
  2. Remove workpieces and scrap stock from machine bed.
  3. Close lid.

### 4. Vinyl cut wings and tails

#### 4.1 Open in Silhouette Studio
1. Open a new project in Silhouette Studio (downloadable here).
2. Input the size of the material you will be working with.
  - Under “page setup,” either select from the dropdown menu under “media size” or manually input the width and height in the text boxes below.
  - The other settings should be fine as-is, but feel free to adjust the media color or rotate the view as desired.
3. Import your .png or .svg file.

#### 4.2 Configure cut settings
1. Navigate to “send” in the main menu (upper right corner of the screen).
2. Select “line” from the top menu. Each line color—-in this case, red and blue--should have its own row.
3. Choose your material for both rows (they will be the same; likely likely "cardstock, plain" although "coverstock, heavy" also works, depending on your material).).
4. Select an action for each row: cut for red and score for blue. (Scoring may be helpful if you wish to add folds or creases in your designs.)
 - If you are using the autoblade tool, you can perform both functions in the same pass. Choose tool no. 1 for both rows. (*Note: your scored lines will be finely perforated rather than scored.*)
 - If you are using two different tools—one for cutting and another for scoring—choose the correct tool number and carriage as appropriate for each row.

#### 4.3 Set up the vinyl cutter
Be sure to set up somewhere with enough room for the mat to feed through. There's no need for a spool feeder for this project, since you'll be using cardstock and a mat.
1. Turn the machine on: long-press the power button on its right side for 2-3 seconds.
2. Connect to the machine via bluetooth or a USB cable.
3. Select the appropriate machine from the menu once it appears.
  - (Note: if a firmware update is required, you will need to install this before proceeding.)
4. Install autoblade (skip this step if already installed):
  - Open the machine’s hood.
  - On the tool carriage, pull the locking mechanism completely out.
  - Place the autoblade into the tool slot, and make sure it is fully inserted.
  - Push the locking mechanism back into place.


#### 4.4 Prepare your material
We'll be using a **low-adhesion mat** for this project.
1. Position your cardstock to align the upper left corner of the material with the upper left corner of the mat and press it firmly into place.
2. Line the mat up with the machine.
3. Push the arrow button to load the mat, centering it between the pressure wheels.
4. Use the arrow buttons to align the toolhead with the upper right corner of where you wish to cut.

#### 4.5 Cut!
Almost done!
1. With your mat lined up and ready, click "send file."
  - The machine will take a few moments to process your file, cutting first and then scoring.
2. When the machine has finished, press the unload arrow to release the cutting mat.
3. To remove your workpiece, peel the mat off of the cardstock instead of vice versa. This helps keep your finished product flat and intact. Peeling the workpiece from the mat will likely result in torn or curled cardstock.
  - Turn the mat over so your workpiece is flat on the table. Then, gently peel the mat off of the workpiece.

### 5. 3D print weights
You can either use our pre-configured, 3D-printable weights or design your own in the CAD software of your choice. Remember that the slot should be sized appropriately for the material(s) you are using.

You have two options to launch your print: 1) send it wirelessly, or 2) us an SD card.
1. Load filament (skip this step if already loaded):
    - Hang filament spool on holder on back of machine so that filament unwinds clockwise.
2. Insert filament into the PTFE tupe on the top back side of the machine.
3. On the machine’s screen, select nozzle → – → bed heat, and head the nozzle to the recommended temperature for the filament (for PLA).
4. Wait for the nozzle to heat up.
5. On the machine’s screen, select nozzle → E → downward arrow several times until the filament comes out of the nozzle.
6. Load file: Select the file icon and select your file to start the print.
- Note: The printer will likely run an automatic leveling check before printing. This usually takes a few minutes.

### 6. Assemble
1. Insert wings through the slot in the plane’s fuselage (body). Make sure they’re centered; the notch will help keep them in place.
2. Slot your tail into place.
3. Optional: add a 3D printed nose weight! (Paperclips or tape and pennies also work).

### 7. Test and Evaluate
1. Stand in the same spot and attempt to apply the same force for each launch.
2. Measure and record the glider’s linear distance traveled after each flight.
  - Consider launching at least three times and calculating the average distance traveled.

### 8. Redesign and Try Again
1. Adjust your design files and try again.
(*Hint: this works best if you adjust only one variable at a time!*)
