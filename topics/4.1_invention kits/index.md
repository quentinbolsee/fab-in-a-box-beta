---
layout: default
title: 4. Cardboard Invention Kits Activity Steps
nav_order: 11
mathjax: true
---


# Laser-Cut Cardboard Invention Kits
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Create your very own invention kit!

In this activity, you will design and laser cut a series of two-dimensional shapes that can be assembled into three-dimensional structures.

Finished kits can be used for brainstorming, rapid prototyping, and more. Use them to build things both imaginative and useful-from decorative artpieces to functional solutions for real-world problems!

### Context

In the real world, engineers and inventors often face tight deadlines for solving problems. It’s easy to get overwhelmed—or to not know where to start!

Invention kits can help facilitate brainstorming and accelerate the prototyping process. They provide a ready-made set of components that can be easily assembled and reconfigured to test out different design concepts or build scale models.

LEGO bricks are a great example of an invention kit. They can be quickly assembled or disassembled in a variety of ways to illustrate a concept or test out an idea. Your invention kits will work similarly, allowing you to build upward or outward—but you get to customize the pieces!

{:toc}

## Materials
- xTool Creative Studio (XCS) software
- xTool Laser Cutter
- Corrugated cardboard
- Calipers



## Activity Steps

### 1. Brainstorm design ideas
Begin by listing possible themes for your invention kit.

#### 1.1 Choose shapes
Consider both decorative and functional shapes that could be useful for building different kinds of structures. Here are a few ideas to help you get started:

- **Basic geometries**: circles, squares, triangles, trapezoids
- **Body parts**: heads, bodies, arms, hands, legs, feet, tails
- **Decorative details**: wings, flowers, bows, hats, mustaches, hair
- **Mechanical elements**: wheels, gears,
- **Infrastructural shapes**: geometries inspired by buildings and bridges

Then sketch your ideas on paper to visualize the shapes and their potential applications.

#### 1.2 Decide on slot placement
Looking at your designs, decide where you want to add slots and/or holes.

- Remember: this determines how your pieces will fit together—but only add a few per piece! Too many, can compromise the strength of your cardboard.

- Add lines or narrow rectangles to your sketch to indicate slot placement.


### 2. Design digitally
You can use any 2D design or 3D CAD software for this step. Although parametric options will make your life easier, the instructions below are for xTool Creative Space (XCS), which is not parametric.
1. **Draw your design**. You can use the XCS shape library, import files from the internet or a different design software, or draw with vectors.
2. **Resize**. You must do this *before* you add slots, since they will be precisely formatted for your material and should not be scaled.
  - Select the design and enter a value in millimeters into the “size” box.
  - You can adjust the width (W) or height (H).
  - By default, the width-to-height ratio will be locked to prevent distortions; click the padlock icon to unlock this if needed.  



### 3. Determine material thickness and kerf
Just like LEGOs, cardboard construction kits rely on a press-fit system to stay together. Their slots must be just the right width for the cardboard they're made from.

In order to engineer your slots correctly, you need to know two values:
  1. **material thickness**
  2. **laser kerf**

*Kerf refers to the width of the material burned away by the laser.*


#### 3.1 Use calipers to measure your cardboard's width
Calipers are a useful tool for precisely measuring material thickness.

1. **Power on**: Press the power button to turn the calipers on.
2. **Zero**: Ensuring the calipers are completely closed, press the zero button. This will reset the measurement to 0.00 mm/inch.
3. **Open calipers**: Gently open the jaws of the calipers slightly wider than the thickness of the cardboard.
4. **Insert cardboard**: Place the cardboard between the jaws of the calipers, making sure it is perpendicular to the jaws for an accurate measurement.
    - Choose an edge or area of the cardboard that is free from creases or damage for the most accurate measurement.
5. **Close calipers**: Slowly slide the jaws together until they touch both sides of the cardboard. (Try not to compress the cardboard; this will lead to inaccurate readings.)
6. **Read measurement**: Read the measurement on the digital display. This will show the thickness of the cardboard in either millimeters (mm) or inches (in).
    - Use the "units" button to switch between millimeters and inches.
7. **Double check**: For accuracy, repeat several times at different points on the cardboard and average the readings.
8. **Write down measurement**: You will need this value in the next step.
- **Tips for ensuring accurate measurements**:
  - **Avoid excess pressure**: Do not apply excessive force when closing the jaws as it can compress the cardboard and result in an inaccurate measurement.
  -  **Clean jaws**: Ensure the caliper jaws are clean and free from debris before measuring.
  -  **Consistent positioning**: Measure at consistent positions to avoid variations in thickness due to structural inconsistencies in the cardboard.

#### 3.2 Determine kerf
Unlike scissors, laser cutters work by vaporizing a tiny portion of the material they’re cutting. The *kerf* is the width of material removed.

Sometimes, when we’re creating something purely decorative, kerf can be ignored. However, when precision matters, so does kerf.

For these construction kits, precision matters. If you don’t compensate for the extra material lost to kerf, your slots will be slightly too wide to hug the cardboard tightly, causing your assemblies to be loose.

1. Cut a small shape out of your cardboard. This shape can be any size and shape, but we recommend keeping things simple with a 25-millimeter square.
2. Use calipers to measure the cutout piece.
3. Subtract this measurement from the intended size in the original design file, or from the size of the hole left behind in the stock material.
4. Subtract the shape's width from that of the hole it left behind.

       Kerf = [width of square as designed] - [width of real square]

- Note that since kerf can vary based on material type, material thickness, and laser cutter settings--even on the same machine--you should always do a test cut for any new project.

#### 3.3 Calculate & test optimal slot width
The optimal width for the slots in your construction kit is determined by two factors: 1) the width of your material, and 2) the kerf.

1. **Calculate slot width**: subtract kerf from material thickness to estimate your slot width:

       Slot width = [measured material thickness] - [kerf]

2. **Test to double check**. Calculations are a great place to start, but it's always best to cut some test pieces to confirm the best fit (and double check your calculations). Since some materials are more compressible than others—meaning they have some spring to them, like cardboard—the ideal slot thickness can still vary.
  - Option #1: Design your own test pieces to cut and measure. (*A comb-like design allows you to cut multiple slots of different widths into the same tester. Consider also engraving the slot's widths directly onto the comb for easy reference.*)
  - Option #2: Adjust or use the comb provided.

  Cut and experiment with your tester to identify the best fit. This will be your slot width.

### 4. Add slots to your digital design
Remember to resize your designs *before* adding slots, since its important to preserve their precise dimensions.
  1. **Draw a slot**.
    - Use the rectangle tool to draw a rectangle.
    - Resize the rectangle to match the desired width of your slots.
    - Duplicate this, and work with the copy going forward. Leave the original alone to use as a tool for future copying & pasting.
  2. **Position slots**.
    - Drag your dimensioned rectangle to position it relative to the shapes composing your invention kit.
    - Place it as desired, using the alignment tools to help with positioning. It's okay to have some overhang; you'll get rid of that shortly.
      - Note: If you need to adjust the length of your rectangle, click the padlock icon to unlock the dimensions (unconstraining width from height) in order to do so. This prevents the width from scaling proportionately.
    - Repeat as appropriate to place all slots around your shape.
  3. **Eliminate overhang and join slot to shape**.
    - Select both your shape and slot(s).
    - Select "combine" -> "subtract".

### 5. Laser Cut Shapes
#### 5.1 Connect laser cutter

 - Turn on the laser cutter and connect it to the computer via USB.
 - Click “connect device” and select the appropriate machine.
 - Load stock material:
      1. Open laser cutter lid and place stock onto honeycomb.
      2. Manually drag laser head over center of stock.
      3. Close lid.

#### 5.2 Auto-focus
  1. Click auto-focus button (this looks like a crosshair symbol next to the distance box).
  2. Wait for machine to focus.
  3. Open lid.
  4. Manually drag laser head to desired position on stock. The red cross will act as a reference point, also appearing in the XCS software.

#### 5.3 Check framing
  1. In XCS software, position your design files as desired relative to the red cross.
  2. Select “framing.” When instructed, press the button on the machine. (This can be done with the machine lid open - there is no laser running during framing.) The laser head will move to map the outer perimeter of the design’s processing area. If this does not fit on the stock or overlaps a previous cut, manually adjust the stock or digitally adjust the design’s position.

#### 5.4 Run the job
  - Click “process” in XCS, followed by the button on the machine when instructed to do so.
  - Important: Never leave the machine unattended while it is running!

#### 5.5 Remove finished pieces
  1. Gently check to make sure all pieces cut through. If they haven’t quite released, you can close the lid and run another cut pass. (Toggle the layer with the engraved design to “ignore” first.)
  2. Remove workpieces and scrap stock from machine bed.
  3. Close lid.

### 6. Assemble!
It's time to slot your pieces together!
What can you create?
