---
layout: default
title: 6. Botanical Press Activity Steps
nav_order: 16
mathjax: true
---

# Fab a Flower Press
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Combine art and science: customize and fabricate a botanical press that can be used to preserve leaves, buds, and blossoms for crafts or dissection.

This activity highlights the enduring appeal of flower pressing while demonstrating how digital fabrication techniques can be used to replicate or enhance age-old tools.


### Context
For centuries, botanists and hobbyists alike have used flower presses to preserve to document plant species, create art, and preserve botanical specimens for scientific study. During the Victorian era in particular, the practice was a popular pastime with pressed flowers used in scrapbooks, cards, and framed artwork.

Today, flower pressing continues to offer a hands-on way to explore plant anatomy, biodiversity, and ecology--or simply to capture memories.

{:toc}

## Materials
- (1) 3mm basswood, plywood, or acrylic - enough to accommodate two 5” x 5” cutouts (face plates)
- (1) sheet of corrugated cardboard - enough to accommodate six 4” x 4” cutouts (inner layers)
- (4) 35mm bolts
- (4-8) washers
- (4) wing nuts (or regular nuts w/ 3D printed wing nut caps)
- (10) sheets of wax paper for between cardboard - optional


## Activity Steps
Your flower press consists of two key laser-cut parts:
  - Two rigid outer face plates
  - Numerous inner cardboard layers

You can also further build upon this activity with these optional add-ons:
  - Creating vinyl-cut "jars" to preserve your specimens, building your very own herbarium.
  - 3D-printing wing nuts to aide in easy tightening and loosening of your press.

### 1. Ideate
Your top and bottom plate are fully customizable. They should share the same shape, be large enough to accommodate a few flowers and leaves per layer, and have room for at least three (ideally four) bolt holes spread evenly around their edges. Beyond these constraints, you can score or engrave whatever decorative elements you like!
- Consider incorporating **text** (typography, calligraphy, quotes, poetry, song lyrics) and **graphics** (botanical imagery, brand logos, images, signatures, etc.)
- Remember to design around the holes in each corner for the bolts that will hold your press together.

#### 2. Design
You have two options for your designs:
 - **Option #1**: You can sketch or hand-draw your design and scan it (or send a photo) into the XCS software.
 - **Option #2**: You can create your design entirely digitally.

#### 2a. For hand-drawn designs
1. Send Your Sketch to the Computer
  - Scan your final design or snap a quick photo from directly above it.
    - Cell phone cameras work fine for this!
  - Send the file to your computer.
    - USB, email, or airdrop are all fine options.

2. Import image into XCS:
  - Click on “image” in the upper lefthand corner and import your design.
   - This works best with an SVG, but you can use any image you want, including jpg or png.
   - Note: If your image is too large, select “yes, scale to fit on canvas.” We’ll be cropping and resizing anyway.

3. Crop away unneeded area:
  - Click “edit image.”
  - Click the “crop” icon and draw a box around the area you want to keep. You can adjust this box after you place it, so don’t worry about being too precise to start with.  
    - Important: when you’re satisfied, **click the small checkmark** at the bottom of the screen.
  - Click “save.”

4. Convert to black-and-white:
  - You need to do this even if you used black marker on a white background, as there is likely to be some residual color from the scanning process.
    - Select “black-and-white” from the bitmap image menu.
    - Adjust the sharpness as desired, and slide the “greyscale” slider all the way to the right (the maximum setting).

5. Trace image to get its outline:
  - Select your design. In the “bitmap image” menu, click the bottom button: “trace.”
  - Adjust the sliders until you’re satisfied.
  - Click “save.” (It’s usually fine to use the preset settings here.) This creates an outline of your design on a new layer.
    - **Note: In order to see this outline, you will need to turn off visibility in the original image layer, which we will do in the next step.**

6. Ignore and hide the original image:
  - In the “layer” menu, select Layer 1. This is your original image.
  - Under the “object setting” menu, click "ignore."
  - Turn off visibility for Layer 1. You should only see your traced outline remaining.
  - (Note: you could just delete this image or the layer it’s on instead of hiding it, but you will not be able to go back and edit it later if desired.)

7. Resize object(s):
  - Select all elements of your design and resize by entering a value in millimeters into the “size” box. You can adjust the width (W) or height (H).
  - Note: by default, the width-to-height ratio will be locked to prevent distortions; click the padlock icon to unlock this if needed.

8. Add a bounding box if needed:
  - If your original sketch didn't include an outer bounding box (the overall shape of your face plates), you should add one now.
    - **Option 1**: Use a simple geometric shape from the shape library.
    - **Option 2**: Draw your own outline using vector lines.

#### 2b. For entirely digital designs
1. You may want to start with your bounding box and then add decorative elements from the XCS shape library or from online.
2. Consider using the design tools to your advantage:
  - **Outline**: Select your desired offset in millimeters. A positive number will give an outline larger than the original object. A negative number will give an outline smaller than the original object.
  - **Array**: Create a gridded or circular pattern of an object.
  - **Group**: Combine multiple objects or design elements to easily move or resize them all at once.
  - **Align**: Control the positions of objects relative to one another. Useful for centering things.
  - **Combine**: Unite or subtract objects to create a visually layered effect.
  - **Reflect**: Flip an object horizontally or vertically.

### 3. Add holes
Don't forget to add the holes for your bolts! These should be evenly spaced around the perimeter of your face plate to apply even pressure to your botanicals.
1. Using the “shapes” menu, select a circle.
2. Resize this circle to the appropriate diameter for your bolts. (*Note: make sure it is slightly larger than the bolt itself–you want a tiny bit of wiggle room so its easy to insert them through multiple layers.*)
3. Position this where you would like one of your holes; this should be somewhere where it will not impede your ability to lay out your botanicals. (Ideally, in a corner).
4. Copy and paste this circle several times, moving each new hole to an appropriate location.
  - You may wish to consider using the “array” function to achieve perfect spacing if your design is a regular, symmetrical shape.
5. Select the outer perimeter and all holes, and navigate to "combine" > "subtract."


### 4. Configure for cutting, scoring, and engraving
Some of your design elements will likely be engraved (fully rastered), while others may be scored (outlined). The outer edge and holes will need to be cut.
  1. Select your material type from the main menu. (We recommend using 3mm plywood for flower presses.)
  2. Select the **outline** and **holes** for your face plate. In the object setting menu, select "cut" as your processing type.
  3. Select all elements to be engraved. In the object setting menu, select "engrave." (Everything that appears black will be engraved; everything white will stay raised.)
  4. Select all elements to be scored (outlined). In the object setting menu, select "score."
  - **Note: You may wish to place each of these categories onto a separate layer for easy reference. With the objects selected, click "move to" and choose a new color. This will move those objects to that layer.**


### 5. Laser cut face plates
#### 5.1 Connect laser cutter
  1. Turn on the laser cutter and connect it to the computer via USB.
  2. Click “connect device” and select the appropriate machine.
  3. Load stock material:
      - Open laser cutter lid and place stock onto honeycomb.
      - Manually drag laser head over center of stock.
      - Close lid.

#### 5.2 Auto-focus
  1. Click auto-focus button (this looks like a crosshair symbol next to the distance box).
  2. Wait for machine to focus.
  3. Open lid.
  4. Manually drag laser head to desired position on stock. The red cross will act as a reference point, also appearing in the XCS software.

#### 5.3 Check framing
  1. In XCS software, position your design files as desired relative to the red cross.
  2. Select “framing.” When instructed, press the button on the machine. (This can be done with the machine lid open - there is no laser running during framing.) The laser head will move to map the outer perimeter of the design’s processing area. If this does not fit on the stock or overlaps a previous cut, manually adjust the stock or digitally adjust the design’s position.

#### 5.4 Run the job
  - Click “process” in XCS, followed by the button on the machine when instructed to do so.
  - **Important: Never leave the machine unattended while it is running!**

#### 5.5 Remove finished pieces
  1. Gently check to make sure all pieces cut through. If they haven’t quite released, you can close the lid and run another cut pass. (Toggle the layer with the engraved design to “ignore” first.)
  2. Remove workpieces and scrap stock from machine bed.
  3. Close lid.


### 6. Design & cut inner cardboard layers
The inner layers of your press are made of compressible cardboard that will gently squeeze flowers flat.

- **Option #1**: These layers can be sized and shaped identically to your press, in which case they will thread over your bolts and need to be removed one at a time.
- **Option #2**: These layers can be slightly smaller than your press, with narrow margins around the bolt holes.

Either option is fine!

#### 6.1 Design inner layers
1. If your inner layers will perfectly match your face plates, simply toggle any decorative elements (engravings & score lines) to "ignore" in the output settings.
2. If you wish to create smaller inner layers, you can select a new shape from the library, draw your own, or use the "outline" function an enter a negative number for "offset distance".
  - Make sure your inner layers don't interfere or overlap with the bolt holes in your face plates.

#### 6.2 Lay out inner layers
1. Copy & paste or use the pattern function to lay out an array of this inner layer shape on your raw material.

#### 6.3 Laser cut inner layers
1. Make sure to switch your material to "3.5mm corrugated paper."
2. Follow the same instructions as above to re-focus the machine and run the job.


### 7. Assemble your press
Assemble your flower press by stacking all its layers together on top of your bolts.
1. Thread a washer onto each bolt.
2. Thread the bolts upward through the holes in the bottom face plate.
3. Add your cardboard inner layers on top of the bottom plate.
4. Finish by threading the top plate onto the bolts.
5. Screw wingnuts onto each bolt and tighten against the face plate, applying even pressure across the press.


### 8. Use your press
Follow the one-in-twenty rule: if there are 20 of a specific flower in bloom, it’s generally okay to take one—if it’s a species that’s not endangered or protected.
1. Arrange your flowers or foliage, ensuring they are spaced evenly and not overlapping.
  - Multiple specimens can be pressed at once if they’re of similar thickness.
2. Add another layer of paper on top of the flowers, followed by another layer of cardboard. You can repeat this process until all of your flowers are sandwiched between layers, or you near the end of your bolts.
3. Once your flowers are arranged, tighten the bolts (or velcro straps, or secure the rubber bands in place) on the flower press to apply even pressure across the face plates.
4. Let the flowers dry for approximately two weeks. (Exact drying time will depend on the thickness of the specimens, the ambient temperature and humidity, and your desired level of drying.)
5. After two weeks, remove the wing nuts, washers and top plate. Then remove your cardboard and paper, layer by layer, to reveal the pressed botanicals between them. Use tweezers to remove these.
