---
layout: default
title: 9. Casting & Molding Activity Steps
nav_order: 22
mathjax: true
---


# 3D Printing for Casting & Molding
By Claire Dorsett
{: .no_toc}

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview
Design and 3D print your very own one- or two-part molds! Use these to cast objects directly, or create a secondary, flexible mold to cast large batches of a design.

### Context
Casting is a manufacturing process in which a liquid is poured or injected into a rigid structure called a **mold** and allowed to cure or solidify. It takes advantage of the properties of liquids; specifically, their ability to conform to the shape of a vessel into which they’re poured.

The special liquids that are poured into molds are called **casting mediums**. These are typically two-part mixtures that undergo a thermal or chemical change called *curing* to harden into a solid, creating items like:

  - Specially-shaped bars of soap
  - Action figures and other plastic toys
  - Fancy chocolates

The individual components must be precisely measured, combined at specific ratios, and mixed together fully in order to work correctly. Otherwise, the mixture will not cure properly, resulting in a wet or soft end product.


#### In the Real World
Sand castles are often made using molds. Wet sand is packed into the forms, which are then carefully removed to leave behind castles in their shapes.

Ice cubes and popsicles are also made made from molds. Water or juice is poured into the forms, which are then frozen.

Plastic toys, clothes hangers, and utensils are also made from casting polymers into specially-designed molds.

#### Key Concepts:
There are two types of molds relevant to this activity (and many more used in real-world manufacturing!).
1. **Single-part molds**: Simple shapes without undercuts can be cast in single-part molds.
  - Straightforward to design, produce, and use
  - Use less material and require less production time
  - Easy to pour and demold
2. **Multi-part molds**: Some shapes are too complex to cast using a single-part mold. These require multi-part molds, in which two or more parts fit together like three-dimensional puzzles, encompassing the object to be cast. Small funnels and air vents allow a casting medium to be poured in, and air bubbles to escape. Once the casting medium has cured, the multiple pieces of the mold are disassembled to provide access to the finished part inside.
  - Allow for complex shapes, undercuts, and intricate details
  - Provide better access to all surfaces of the object
  - Can be designed along natural seams or edges for the object being cast to minimize visible lines on the finished casting

##### Key Terms:
  - **Mold**: The rigid frame into which a casting medium is poured.
  - **Casting Medium**: The material you are pouring into a mold. This undergoes a chemical or physical change to harden from a liquid into a solid, and often gets mixed together from two parts.
  - **Cast**: To pour a casting medium into a mold; the solid form that comes out of a mold.
  - **Curing**: The chemical process a casting medium undergoes while hardening.
  - **Registers**: In multi-part molds, registers are design features that interlock to keep the mold precisely aligned during pouring and curing.
  - **Sprue**: In multi-part molds, sprues are like small, funnel-like openings into which the casting medium is poured.
  - **Vent**: In multi part molds, vents are small air shafts that allow air bubbles to escape from the interior cavity, preventing empty spaces in the finished product.

{:toc}


## Materials
- Computer with CAD software
- 3D printer
- PLA in a color of your choice
- Casting mediums (any or all)
  - Playdough
  - Hydrostone
  - Oomoo
  - Wax - will also require a crucible or pan and hotplate
- Mold release agent (optional)
- Mixing tools:
  - Stirring sticks (popsicle sticks or tongue depressors)
  - Mixing cups (paper cups)
-Measuring tools:
  - Small scales
  - Disposable volumetric measuring cups
- Safety equipment:
  - Gloves
  - Eye protection
  - Lab coats or aprons
  - Close-toed shoes for all participants


## Activity Steps
There are a few different options for this activity.

### 1. Ideate
First, determine what you will cast and how.
1. Choose an object you wish to cast. This can be something you will model CAD (or downloaded from the internet, with requisite copyright permissions), or a real-world item you want to replicate.
2. Consider the object’s geometry. Does it have undercuts that will necessitate a multi-part mold?
3. If you are making a mold of an existing object, determine what it is made of.
  - Some plastics will react with or otherwise inhibit the curing process for certain casting mediums. (For example, Sortaclear will meld with Oomoo). Be sure to read your data sheet!
  - Determine whether your object floats or sinks in the casting material, and develop a plan for how to keep it in place during casting. (Fine thread or fishing line can help you suspend or submerge an object as needed).
4. Determine how many **parts** and how many **steps** your mold will require.
  - Single-part molds have only once piece, with the casting agent poured directly into their top.
  - Multi-part molds have multiple pieces (usually two halves) that fit together with small, funnel-like sprues and vents left open to accommodate the casting agent and release displaced air.
  - Single-step molds are 3D prints that are cast directly into.
  - Multi-step molds will require you to 3D print a mold that is then used to cast a second, more flexible mold from. It can be easier to remove objects from this secondary mold, but creating it will require you to think critically about your design geometries along the way.

### 2. Design mold
You can use any CAD or 3D design software you like for these steps. While specifics will depend on which software you choose, the steps outlined below offer a high-level overview of the design process.

**Note**: While the initial steps are the same, later steps vary slightly depending on whether you are creating a **one-step** or **two-step** mold.

#### 2.1 Model the finished object
1. First, create (or import) a CAD representation of the object you wish to cast. We will design the mold around this.

#### 2.2 Model a mold around the object
Steps here vary slightly, depending on whether you are creating a multi-step or multi-part mold. Please note the subsection labels.

##### 2.2a For a *one-part, one-step* mold
1. Model a three-dimensional box that fully encompasses the face of the object you wish to replicate and is slightly larger in each dimension.
2. Use the original object to “cut” into this box.

##### 2.2b For a *two-part, one-step* mold
1. Model a box slightly larger than the object (~1cm in in each direction) with the object centered within it.
2. Select the object and use it as a tool to “cut” the box. This will leave you with a cavity inside.
3. Create a center plane between two faces of the box so that it splits your object—and the box—in half.
4. Use the plane you just created to “cut” or split apart both the object and the box.
5. Add a sprue and vent. Choose the side of the mold you wish to pour your casting medium into, and add two small, parallel cylinders (no smaller than~3mm in diameter) running from the inner cavity to the outside of the mold.
6. Use these cylinders as tools to “cut” both halves of the mold. You should be left with small channels.
7. You may wish to add registers to help the two halves of your mold fit together more precisely.
  - Small hemispheres in diagonally opposite corners works well for this. The hemispheres on one half of the mold should be positive (*convex*) while those on the other should be negative (*concave*). Consider making the concave hemispheres ~.5mm larger in diameter than the convex ones to add a little buffer.

##### 2.2c For a *one-part, two-step* mold
1. Create a plane or new sketch on the opposite side of the face you wish to replicate.
2. Sketch a rectangle slightly larger han the object’s footprint.
3. Create an offset line approximately 3mm outside of the rectangle you just made. This should create a margin all the way around your object.
4. Extrude the margin a distance slightly greater than the object’s height. (~3-5mm).
5. Extrude the back of the box (margin included) ~3-5mm.
6. Join all of the bodies together. You should now have a hollow box with your object on the bottom.

##### 2.2d For a *two-part, two-step* mold
1. Create a center plane between two faces of your object, splitting it in half alone your desired seam.
2. Use the plane you just created to “cut” the object, dividing it into two halves. Rotate one of these halves so that both are side-by-side on the same plane.
3. For each half of your object, create a “bed.” This should be a box around it with ~3mm-5mm-thick walls.
4. Add a sprue and vent. Choose the side of the mold you wish to pour your casting medium into and add two small, parallel cylinders (no smaller than~3mm in diameter) running from the object to the outside of the mold.
  - Make sure these are in the same spots on both halves!
5. Use these cylinders as tools to “cut” both halves of the mold. You should be left with small channels.
6. You may wish to add registers to help the two halves of your mold fit together more precisely.
  - Small hemispheres in diagonally opposite corners works well for this. The hemispheres on one half should be positive (*convex*) while those on the other should be negative (*concave*). Consider making the concave hemispheres ~.5mm larger in diameter than the convex ones to add a little buffer.

### 3. Prepare & slice files
1. Open your slicing software: Bambu Studio
  - Slicing softwares, often called “slicers,” are used to prepare .stl files for 3D printing. They offer tools and workflows to help you lay out multiple bodies on a single print bed, add supports, and more.
2. Import your design into the slicer. Use the file icon or drag and drop.
3. Select the type of printer you’re using (P1S).
4. Select the bed type.
5. Select the filament type being used (PLA).
6. Select the slicing settings.
7. Click “slice.” This will create a .3mf file and take you to a preview window that shows you what your finished design looks.

Your dice is now ready to print!

### 4. 3D print mold
You have two options to launch your print: 1) send it wirelessly, or 2) us an SD card.
1. Load filament (skip this step if already loaded):
    - Hang filament spool on holder on back of machine so that filament unwinds clockwise.
2. Insert filament into the PTFE tupe on the top back side of the machine.
3. On the machine’s screen, select nozzle → – → bed heat, and head the nozzle to the recommended temperature for the filament (for PLA).
4. Wait for the nozzle to heat up.
5. On the machine’s screen, select nozzle → E → downward arrow several times until the filament comes out of the nozzle.
6. Load file: Select the file icon and select your file to start the print.
  - Note: The printer will likely run an automatic leveling check before printing. This usually takes a few minutes.
7. Once the printer is done, pop your mold off the bed. If it seems stuck, gently use a soft prying tool (a 3D printed one works well!), or remove the magnetic bed from the printer and gently flex it to help the mold release.

### 5. Prepare mold for casting
There is some setup required before casting.
1. Although not required, you may wish you spray a mold-release agent into your mold. This can help the finished product slide out more easily.
2. If using a multi-part mold, fit both halves together. Use clamps or rubber bands to keep them firmly joined. Make sure your sprue and vents are clear.
  - Note: if you have many tiny details, you may wish to wait until *after* mixing your casting medium before assembling your mold. Painting a thin layer of the medium into an nooks and crannies before sealing it up can help prevent air bubbles.

### 6. Cast
Here's where the (messy) magic happens.

A few important notes:
- NEVER pour casting materials down the drain or into any plumbing. These can cure and solidify in the pipes, causing extensive (and expensive!) damage.
- Many curing processes are **exothermic**! That means they get hot; the casting medium releases heat as it solidifies. This can potentially be dangerous if it occurs in a small, confined space or near something flammable.
- **Leave excess mixed mediums alone in their cups to cure fully *before* disposing of them.**



#### 6.1 Prepare workspace
1. This is a messy process! Lay mats, newspapers, paper towels, or another protective layer across your workspace before beginning.

#### 6.2 Choose your casting medium
1. Choose your casting material based on the properties you want your finished piece to have. Some examples:
  - Hydrostone yields a hard, white, brittle finish.
  - Oomoo yields a malleable, soft, bluish finish.
  - Sortaclear ranges in firmness and maleability, but is considered food safe (assuming the rest of the equipment used was also food safe).

#### 6.3 Mix your casting medium
1. Following the instructions on your casting medium’s data sheet, mix together Parts A and B in the ratios directed.
  - These may be measured by volume (using measuring cups or graduated cylinders) or by weight (using a scale).
  -You will have a limited amount of time to fully mix the material and pour it into your mold once the two parts combine. On the data sheet, look for “working time.”
2. Once you’ve combined parts A and B, use a stirring tool like a popsicle stick or tongue depressor to gently mix the two together.
  - Do this **slowly** to avoid introducing air bubbles.
  - Try to avoid folding the material over onto itself, as this also mixes air in.
  - You will know you’re done when the medium looks *homogenous*, meaning the same throughout. You shouldn’t have any chunks, or marbling of colors.

#### 6.4 Pour
1. Slowly and carefully, pour your casting medium into your mold through the sprue. If your sprue is large enough, you can also use a funnel.
2. Keep filling until your mold begins to overflow. Then give it a few taps on the table to help any air bubbles rise, and pour a bit more material in if space allows.
3. Repeat this until no further air bubbles surface.

#### 6.5 Clean up
1. Clean up any spills before they have a chance to cure.
2. Leave excess casting materials in their mixing cups to cure before disposing of them.
3. **Remember, NEVER put any casting materials down the drain!**

#### 6.6 Wait to cure
1. Now you wait! Your data sheet will tell you the approximate curing time for your casting medium.

### 7. De-mold
1. After waiting the appropriate curing time, remove any clamps or rubber bands from multi-part molds and carefully open them.
2. Trim off any seams or overfill from the sprue or vent.

### 8. For multi-step molds: casting a finished product
You have just used a mold to cast another mold! This new mold is likely more flexible than a rigid 3D print, allowing for easer removal of finished objects.

To cast your finished object, repeat the instructions outlined in Step #6.
