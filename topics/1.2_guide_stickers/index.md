---
layout: default
title: 1. Vinyl-Cut Stickers Facilitator Guide
nav_order: 6
mathjax: true
---


# Faciltator Guide: Design and Cut a Custom Vinyl Sticker
By Claire Dorsett and Racheal Naoum
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Learners will design and fabricate their very own custom stickers!

These can be hand-drawn or digitally designed before being formatted for cutting in the Silhouette Studio software.

This activity offers a fun introduction to 2D design and the vinyl cutter, a versatile fabrication machine.

### Context

Vinyl cutting appears in projects big and small, ranging from custom decals for laptops and water bottles to whole-body car wraps and full-wall murals. It can also be used to make custom stencils and labels.

It is a type of subtractive fabrication, meaning we start with a full sheet or roll of vinyl and remove—or subtract—the parts we don’t need.

{:toc}


## Scalability
### 1. Exposure: Hand-Drawn Stickers  
Design & cut a hand-drawn sticker design.
  - Template (outline) provided.
  - Size constraint: 3.75 inches squared.

### 2. Exploration: Digitally Designed Stickers
Design, configure, and cut a CAD-created sticker design.
  - Can incorporate elements of SVG or PNG files from online.
  - Size constraint: 3.75 inches recommended (for easy batch fabrication).

### 3. Deep Dive: Multi-Layered Stickers
Design & fabricate complex, multi-layered sticker designs using registration marks.
  - Multi-colored and/or multi-layered stickers using registration marks.
  - Invites more complex designs.
  - No size constraints.

### Extension: Vinyl-Cut Circuits
Vinyl cut copper tape to make customized, flexible circuits (that are even solderable!) These can be used to liven up greeting cards or scale models by adding coin batteries and LEDs, speakers, or small motors.


## Materials
- Silhouette Studio or other 2D design software
- Vinyl cutter
- Vinyl sheets or roll in the color(s) of your choice
- Transfer tape or transfer paper
- Weeding tools: picks and tweezers
- Squeegee or plastic card


## Standards Alignment
- Next Generation Science Standards
  -
- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will / will be able to…
1. Consider shape and composition when designing in 2D.
2. Identify, discuss, and apply key principles of graphic design like balance, symmetry, and negative space to tell a story.

### Fabrication goals
Learners will be able to…
1. Design in 2D.
2. Vinyl cut a design they’ve created.
3. Weed and transfer a vinyl-cut design.


## Facilitator Notes
### 1. Setup & pre-preparation
- (Optional) Print handouts for each learner to aid in the brainstorming process.
- You may wish to pre-load the vinyl into your cutter; however, we recommend doing this as a demonstration for the larger group, to help familiarize learners with the mechanical operations of the vinyl cutter.

### 2. Batch cutting
  - This activity is optimized for batch-cutting to economize time and materials; however, you can also cut one learner’s design at a time (or any other number) if that’s more appropriate for your setting.
  - If using the batch-cutting template (optimized for xDesign), each user account can be renamed so they match with a sticker model (01, 02, and so forth), so “Person 01” knows to work on “Sticker 01”, etc. All models are saved to a shared collab space.
  - When finished, ask learners to color their models red so that you have a visual indication that they are done.

### 3. Time management
  - Try to split the lesson into 30 minutes modeling, 30 minutes cutting/working on sample stickers, and whatever time remains after the other two undoubtedly go long to pass out the cut stickers for learners to take home. Time management really depends entirely on the ability of the learners and the complexity of their designs.

### 4. Design considerations to share with learners
- Remain within the bounds of the 3.75-inch sticker base provided, and do not edit it to be larger (it won’t fit on the vinyl).
- Use geometry larger than a 0.1 inch (2.54mm) circle. Cuts that are too small can easily get messed up and be difficult to “weed”.
- Minimize the number of “islands” in the design. They add complexity to the transferring process.


## Resources
- templates in xDesign (batch-cutting)

## Discussion questions


## Evaluation Rubrics
