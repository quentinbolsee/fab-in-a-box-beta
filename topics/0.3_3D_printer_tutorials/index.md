---
layout: default
title: 3D Printer Tutorials
nav_order: 4
mathjax: true
---

# General Tutorials
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## 3D Printing
### Bambu P1S 3D Printer
#### Quick Start Guide
Available from Bambu here: https://bambulab.com/en/support/documentation

#### Connecting the 3D printer


### Bambu Studio (Slicer) Software Tutorials
#### Downloading Bambu Studio
Download Bambu's slicer software, Bambu Studio, for free at: https://bambulab.com/en/download/studio
