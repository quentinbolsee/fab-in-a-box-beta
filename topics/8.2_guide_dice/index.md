---
layout: default
title: 8. Custom Dice Facilitator Guide
nav_order: 21
mathjax: true
---


# Faciltator Guide: Print Custom (or Weighted!) Dice
By Claire Dorsett

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}

# 3D Print Custom (or Weighted!) Dice
{: .no_toc}

## Overview

Learners will design their own 3D-printable dice.

They will first choose their die's geometry (cubic or polyhedral), and then decide what to put on each face. Dice can be either traditional (numbered) or creatively themed (decorated with text or symbols).

This activity introduces learners to the "extrude" command in CAD software, as well as to slicer software to format 3D print files. Advanced learners can experiment with adding hidden asymmetry inside a hollow object to create weighted "trick" dice.

Finished dice can be used as story-starters in English class, to run experiments investigating statistics, or as part of a larger activity on game design.


### Context
The earliest known dice, excavated from Mesopotamian tombs and dating back to around 3000 BCE, were made from materials like bone, wood, and stone. Historians speculate that these were used not only in games of chance, but also for decision-making. Later, in ancient Greece and Rome, dice games were popular pastimes.

The fairness and randomness of dice are crucial to maintaining the integrity of games of chance. A fair die is one that has an equal probability of landing on any of its faces. A standard six-sided die should have a 1 in 6 chance of landing on any outcome on any given roll. This requires significant precision in modern-day manufacturing processes to balance the weight and shape of the die. Any imperfections, such as asymmetrical faces, uneven weight distribution, or air bubbles within the material can bias the roll, making certain outcomes more likely than others.

To assess whether dice are truly fair and random, statistical tests and experiments are often conducted. These involve rolling the dice a large number of times and recording the results to determine whether each face appears with roughly equal frequency.

Beyond gaming, the principles of probability explored through dice have practical applications in fields like computer science, where random numbers must be generated for simulations and to encode messages.

{:toc}


## Scalability
### 1. Exposure: Custom Cubic Dice
Design and customize traditional, cubic dice.

- Introduces the "extrude" and "fillet" commands in CAD (and optionally "draft" and "fillet").
- Investigates and compares both proud (positive distance) and recessed (negative distance) extrusions.
- Plays with the surface features of dice.
- **Curricular tie-ins**: math, geometry, statistics, english language arts, social-emotional learning
  - Use dice as story-starters, with different words or parts of speech on each face as creative prompts for English lessons.
  - Invite learners to create their own mindfulness aids, with different meditation techniques or breathing exercises on each face.
  - Explore the probability of different outcomes when rolling dice, and ask learners to calculate and analyze the likelihood of rolling specific numbers or combinations.


### 2. Exploration: Complex Geometries & Polyhedra
Design complex polyhedral dice with many sides and learn about their geometries.

- Introduces designing complex three-dimensional geometries in CAD.
- Introduces polyhedra, including how to name polyhedra.
- Investigates and compares both proud (positive distance) and recessed (negative distance) extrusions.
- **Curricular tie-ins**: math, geometry, statistics
  - Consider (or calculate) how the odds change when additional faces are added to a die's geometry.
  - Introduce Euler's formula, which states that the number of Faces (F) and vertices (V) added together will always be exactly two more than the number of edges (E) a polyhedra has.

### 3. Deep Dive: Engineering Weighted "Trick" Dice
Design custom "loaded" dice with invisible internal weights to skew the odds.

- Encourage learners to be intentional about which face(s) they want to favor, and to design accordingly.
  - Include a cutaway image or sketch in deliverables to that show the design's interior.
- Run probability tests (multiple trials) to see how successfully learners have skewed the odds.
  - Have them identify a goal probability before beginning the assignment (just enough to favor their odds; not enough to be 100%) and then test to see whether their real-world results match this target. If time allows, invite iterative design to try to meet the target.
- **Curricular tie-ins**: math, probability, statistics
  - Explore experimental vs. theoretical probability: compare the outcomes of experimental data (actual rolls) with theoretical predictions.
  - Ask learners to swap their dice without revealing which outcome is weighted, and see if their partners can guess based on 3, 5, or 10 rolls. Have them consider (or calculate) how many rolls, at a minimum, they would need in order to correctly ascertain which outcome is weighted.

### Extension: Accessible Game Design
Dice can be woven into a larger, multi-session unit about accessibility, where learners can (re)design board games, in general or for someone with a disability.
- Session 1: Design game board or game cards
- Session 2: Fabricate game pieces
- Session 3: 3D print dice
- Session 4: Vinyl cut packaging / box / card decals



{:toc}

## Materials
- Computer with CAD software
- 3D printer
- PLA filament in a color of your choice


{:toc}

## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Design an object in three dimensions.
2. Think critically and creatively about how to lay out their dice.
3. Make decisions about what features to include on each face.
4. Determine the probability of an occurrence based on multiple trials.
5. Calculate probability as a fraction, decimal, and percentage.
6. Compare theoretical with experimental outcomes through testing.




### Fabrication goals
Learners will be able to…
1. Use CAD to translate an idea into 3D design for 3D printing.
2. Add design elements to a 3D body’s faces.
3. Prepare & slice a file for 3D printing.
4. Add supports as needed for 3D printing.
5. Use the 3D printer to fabricate an object of their own design.





## Facilitator Notes
### 1. Setup & pre-preparation
- Collect a variety of dice and/or other polyhedra to look at together as examples.
  - Practice naming 2D geometries and the 3D polyhedra comprising them.
- Print or design optional handouts for each learner to help with brainstorming.
- Discuss polyhedra and the relationships between two-dimensional and three-dimensional ones.
- For older or more advanced learners, this activity could be run iteratively, with learners printing two or more designs to experiment with different weights and weight distributions (infills) and design features (proud versus engraved designs, rounded corners, etc.)

### 2. Brainstorming & paper prototyping
- Have learners list the design elements they want to include on their die's faces (use a numbered list with as many lines as there are faces).
- Since three-dimensional geometries can be tricky to visualize, it can be helpful to start by creating paper prototypes.
  - Provide or have learners make 2D, flattened views to map layout of faces so they can visualize which ones will be next to one another. They can then cut these out and fold them into dice.
    - Alternatively, if no 2D maps are available, learners can cut out as many shapes as their final die will have and tape them together one-by-one.
  - Ask learners to sketch multiple perspectives of the finished, 3D product.
    - This helps teach spatial thinking.

### 3. Batch processing
- Learners’ designs can be batched together for printing, with the prints launched during a reflection period or overnight. Depending on their sizes, 25+ dice can typically fit on a single bed.   

### 4. Design considerations to share with learners
- How the the numbers/features on a face are extruded (whether they are proud and sticking up from the face or recessed down into it) can effect how the die will roll.
  - Proud features may interfere with the rolling.

### 5. Themes
- Dice can be used for more than numbers! Incorporate this activity into your existing curricula by adjusting its theme accordingly.
  - **Story starters**: Make a set of multiple dice, with one dedicated to a different story component (genre, setting, character, conflict, hero, villain) or part of speech (nouns, verbs, adjectives, adverbs). Once fabricated, roll the dice and write, tell, or act out a short story that incorporates the words and concepts that appear. (Stories should include a beginning, middle, and end!)
  - **Decision makers**: Take inspiration from the Magic 8-Ball! Write a different decision on each face and let the dice decide for you: yes, no, maybe, roll again, etc.
  - **Mindfulness aids**: Put a different breathing exercise or calming technique on each face.
  - **Tricksters**: How might you adjust your design to favor one or two outcomes over the others? Can you make this invisible? What about undetectable entirely?
What happens if you put a small sphere or other weight into your cube as it prints?

## Extension ideas
1. **Experimental inquiry**:
  - Ask learners to design experiments to compare theoretical (expected) versus experimental (real-world) results from their dice, rolling a set number of times and recording and/or graphing the results.
2. **Trickery Revealed**:
  - Have learners swap their weighted dice without revealing to one another which face is weighted.
    - Challenge them to determine the weighted side in as many rolls as possible, and to then test their theories.
    - Discuss the math behind the fewest rolls needed to determine probability.
3. **A Delicate Balance**
  - Offer advanced learners to an iterative design challenge: to design and fabricate a weighted die that lands on the weighted side with only a specific degree of frequency. This requires subtle manipulation of the interior weight distribution, and much iterative testing to match real-world statistical results to the goal frequency.




## Discussion questions
- **General**
1. How could iterative testing improve the functionality and fairness of your custom dice?
2. What criteria did you use to evaluate the success of your dice design?
3. How do the principles of geometry and probability apply to real-world scenarios like games and decision-making tools?

- **Math & Geometry**
1. How does the shape and number of faces on a die affect the probability of rolling a specific number or symbol?
2. What are the differences in probability between rolling a standard six-sided die versus a polyhedral die with more faces (for example, 10)?
3. How can you calculate the theoretical probability of each outcome for your custom die?
4. What is the minimum number of rolls needed to statistically determine if a die is fair or loaded?

- **Experimental Design**
1. How did/could you design and conduct an experiment to compare the theoretical probability of each outcome with the experimental results?
2. How can you use statistical methods to determine if your dice are fair or biased?
3. How can you represent the data from your dice rolls using graphs or charts to visualize the probability distribution?nd friction influence the performance of a spinning top?

- **Creativity & Problem Solving**
1. What challenges did you encounter during the design and fabrication process, and how did you overcome them?
2. What creative techniques or tools did you use to when designing your dice?
