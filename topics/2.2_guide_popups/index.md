---
layout: default
title: 2 Paper Pop-Ups Facilitator Guide
nav_order: 8
mathjax: true
---


# Faciltator Guide: Design, Cut, and Fold Paper Pop-Ups!
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Learners will use a vinyl cutter to make two-dimensional artworks “pop!” by adding score and cut lines to digital designs.

Pop-ups can be as simple as custom nametags or as complex as greeting cards or storybook scenes.

This activity introduces the basics of paper engineering and the art of using mountain and valley folds, as well as teaching basics around the vinyl cutter and 2D design.

### Context

Paper pop-ups are a form of engineering that use basic forces—simple pushes and pulls—to fold and unfold dynamic three-dimensional scenes from simple cardstock.

Some of the earliest pop-ups were used to make books and other texts interactive, inviting readers to lift flaps, pull tabs, and unfold elaborate spreads tucked into the pages.

Today, paper engineering is often used to make greeting cards and decorations "pop"!

{:toc}



## Scalability
### 1. Exposure: Cutting and Creasing Pop-up Nametags
Cut, crease, and fold a custom pop-up nametag.
  - Single color / sheet of cardstock.
  - Uses letters only; allows learners to learn how to add cut and score lines on letters to create a desired effect.
  - Introduces Silhouette Studio and the vinyl cutter.
  - **Curricular tie-ins**
    - Engineering / design: introduce mountain and valley folds.

### 2. Exploration: Designing Dynamic Greeting Cards
Design and fabricate a custom pop-up greeting card.
  - Multiple colors / layers of cardstock.
    - This version requires two rounds of cutting: one for the pop-up backer, and another to cut out the features that get glued on top.
  - Scalable for different ability levels, simple to complex.
  - More open-ended: allows focus on illustrating a feeling or theme.
  - **Curricular tie-ins**
    - ELA or foreign languages: invite learners to write customized messages for their cards.

### 3. Deep Dive: Complex Engineering for Pop-Up Books
Design & fabricate complex, multi-layered sticker designs using registration marks.
  - Multiple colors / layers of cardstock.
    - This version requires two rounds of cutting: one for the pop-up backer, and another to cut out the features that get glued on top.
  - Tie-ins to existing pop-up art/books like *123D*.
  - Can be done individually or in groups: each learner/group illustrates one scene of a larger story.
    - For extended units: fabricate an entire short pop-up book
  - **Curricular tie-ins**
    - Engineering: invites the incorporation of other paper-driven mechanisms (e.g. pull-tabs, paper levers, etc.)
    - ELA / history: investigate illustrating a scene in literature or history.


### Extension: Incorporating Paper Circuits
Add copper tape, button batteries, and LEDs to make light-up circuits!
  - No soldering required = appropriate for any level!
  - Add copper tape, a coin cell battery, and LEDs to really bring pop-ups to life.
    - Consider adding a press-tab; when two copper-clad paper “buttons” are pressed together, it closes a circuit to illuminate the bulb.


## Materials
- Silhouette Studio or other 2D design software
- Vinyl cutter
- Cardstock
- Decorative craft materials - googly eyes, feathers, sequins, etc.


{:toc}


{:toc}


## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas:
    - Defining and Delimiting Engineering Problems (3-5-ETS1.A),
    - Developing Possible Solutions ((3-5-ETS1.B),
    - Optimizing the Design Solution (3-5-ETS1.C)
  - Science and Engineering Practice:
    - Planning and Carrying Out Investigations (3-5-ETS1-2);

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Demonstrate an understanding of the principles of paper engineering, including cuts, creases, and mechanisms.
2. Apply spatial reasoning in two and three dimensions.
3. Define, differentiate between, and manipulate mountain and valley folds.
4. Understand the roles pushes and pulls play in paper pop-ups and provide examples of similar forces at work in the real world.


### Fabrication goals
Learners will be able to…
1. Prepare a file for the vinyl cutter, applying appropriate settings for both scoring and cutting.
2. Use the vinyl cutter to create scores and cuts, including loading material onto and removing it from a cutting mat.
3. Conceptualize and manipulate mountain and valley folds.
Use tools to help direct and reinforce creases.



## Facilitator Notes
### 1. Setup & pre-preparation
- (Optional) Print handouts for each learner to introduce key concepts like mountain and valley folds.
- Create an **example** to illustrate how the finished products will work.
  - Preparing multiple versions of this example to illustrate it at various points along the fabrication/assembly process can help learners understand the complexities of mountain and valley creasing more easily.
- Consider 3D printing some simple tools to assist with folding and creasing (files provided).
- You may wish to pre-load the vinyl into your cutter; however, we recommend doing this as a demonstration for the larger group, to help familiarize learners with the mechanical operations of the vinyl cutter.

### 2. Batch cutting
  - This activity is optimized for batch-cutting to economize time and materials; however, you can also cut one learner’s design at a time (or any other number) if that’s more appropriate for your setting.
  - If using the batch-cutting template (optimized for xDesign), each user account can be renamed so they match with a sticker model (01, 02, and so forth), so “Person 01” knows to work on “Sticker 01”, etc. All models are saved to a shared collab space.
  - When finished, ask learners to color their models red so that you have a visual indication that they are done.

### 3. Time management
  - Try to split the lesson into 30 minutes for design and file prep, 30 minutes for cutting, and 30 minutes for creasing and folding.

### 4. Design considerations to share with learners
- Use only **capital letters** and **bold, blocky fonts**.
- For especially **long names** that may not fit into the recommended template size, you can either A) reduce the font size until the name fits, or B) cut this name as a one-off outside of the batch run.

### 5. Prototyping
- If prototyping with printer paper, we had success with the following settings:
   - Material: choose "cardstock, plain" even when using printer paper (the others don’t have the option to score)
    - **Cut**:
      - Passes: 1
      - Force: 20
      - Speed: 4
    - **Score**:
      - Blade depth: 1
      - Passes: 1
      - Force: 6
      - Speed: 2



## Discussion questions
- **Art & Design**:
1. How can different colors and textures of cardstock impact the final look of your pop-up design?
2. What are some design elements that work well in a pop-up format? Why?
3. How does the use of negative space (areas without cuts or scores) affect the overall design?

- **Science: Form & Movement**:
1. What basic principles of physics (pushes and pulls) are involved in making your pop-up work?
2. How do mountain and valley folds contribute to the movement and structure of pop-up elements?
3. Can you think of other real-world applications where similar folding mechanisms are used?

- **Math: Geometry & Origami**:
1. How do geometric shapes and angles play a role in the design of pop-up mechanisms?
2. What mathematical principles can be observed in the creation of folds and creases?
3. How can understanding symmetry and asymmetry help in designing effective pop-up cards?

- **English Language Arts or Foreign Languages (Greeting Card Version)**:
1. How can the design of a pop-up card enhance the message you are trying to convey?
2. In what ways can pop-up cards be used to communicate emotions or themes in a unique way?
3. How might the process of writing a letter in another language change your approach to designing a pop-up card?

- **Engineering & Technology**:
1. What challenges did you encounter when using the vinyl cutter, and how did you overcome them?
2. How does the precision of digital design software contribute to the accuracy of your pop-up creation?
3. In what ways can iterative testing improve the final design of your pop-up card?

- **Creativity & Problem Solving**:
1. What creative techniques did you use to make your pop-up design stand out?
2. How did you solve any problems that arose during the cutting and folding process?
3. What other materials or tools could you use to enhance your pop-up cards?
