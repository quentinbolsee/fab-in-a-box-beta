---
layout: default
title: Laser Cutter Tutorials
nav_order: 2
mathjax: true
---

# General Tutorials
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}


## Laser Cutting
### xTool S1 Laser Cutter

#### Connecting the laser cutter

 1. Turn on the laser cutter and connect it to the computer via USB.
 2. Click “connect device” and select the appropriate machine.
 3. Load stock material:
  - Open laser cutter lid and place stock onto honeycomb.
  - Manually drag laser head over center of stock.
  - Close lid.

#### Auto-focusing
  1. Click auto-focus button (this looks like a crosshair symbol next to the distance box).
  2. Wait for machine to focus.
  3. Open lid.
  4. Manually drag laser head to desired position on stock. The red cross will act as a reference point, also appearing in the XCS software.

#### Framing
  1. In XCS software, position your design files as desired relative to the red cross.
  2. Select “framing.” When instructed, press the button on the machine. (This can be done with the machine lid open - there is no laser running during framing.) The laser head will move to map the outer perimeter of the design’s processing area before returning to its starting point.

  If this does not fit on the stock or overlaps a previous cut, manually adjust the stock or digitally adjust the design’s position.

<video autoplay loop controls><source src="media/video/framing.mp4" type="video/mp4"></video>

[![](media/img/bokeh.jpeg)](media/img/bokeh.jpeg)

#### Running a job
  - Click “process” in XCS, followed by the button on the machine when instructed to do so.
  - **Important: Never leave the machine unattended while it is running!**

#### Removing finished pieces
  1. Before removing pieces from the bed, gently check to make sure they have cut through:
    - Hold the material in place.
    - Gently use a fingernail or small tool to test whether the cut pieces will lift free from the stock material.
    - If they won’t quite released, you can close the lid and run another cut pass. (**Toggle any layer's with the engraved design to “ignore” first!**)



### XCS Software Tutorials

#### Downloading XCS
- xTool’s free Creative Space (XCS software) is downloadable here.

#### Selecting materials
- Many commonly used materials will appear in the drop-down menu. Selecting a material here will automatically populate xTool’s recommended settings for score/engrave/cut processes.
- If you are using a less common material like laser-safe stamp rubber, you will likely need to select “user-defined material” and manually enter your own settings for power and speed.

#### Designing
- XCS allows you to use a combination of text, shapes, and vectors to translate your idea into the digital world.
  - Choose from the XCS shape library if you want a place to start.
- You may often want to focus on composition before sizing. You can resize everything at once later.

#### Importing an image
1. Click the folder icon.
2. Select "import."
3. Select your desired file.
4. A pop-up may suggest that you resize a file to fit on the xTool canvas. Click "yes" to do so.

#### Formatting for cutting, engraving, or scoring:
- Choose the relevant objects you wish to format.
  - If everything within a single layer will be processed the same way, you can save time by selecting that layer from the layers menu.
- Under processing type, choose “cut,” "score," or "engrave."
  - Most materials will autopopulate with recommended settings for power, speed, and number of passes. You can also enter settings manually as needed.
  - If your material is not cutting through, consider increasing the power or number of passes.
- Repeat this process for all objects / processes in your design.
  - You can run multiple processes within the same job (e.g. cutting, scoring, and engraving all at once).

#### Useful tools and their applications
- **Outline**: Select your desired offset in millimeters. A positive number will give an outline larger than the original object. A negative number will give an outline smaller than the original object.
- **Array**: Create a gridded or circular pattern of an object.
- **Group**: Combine multiple objects or design elements to easily move or resize them all at once.
- **Align**: Control the positions of objects relative to one another. Useful for centering things.
- **Combine**: Unite or subtract objects to create a visually layered effect.
- **Reflect**: Flip an object horizontally or vertically.

#### Grouping & ungrouping objects
- Once you’re happy with a design's composition, you can group all objects together and manipulate them as a single unit
  - Select all objects > right click > group.
- Follow the same process to ungroup objects if you need to manipulate them independently again.
- **Note: Some commands like joining and subtracting may not work on grouped objects.**

#### Resizing an object
- Select the design and enter a value in millimeters into the “size” box.
  - You can adjust the width (W) or height (H).
  - By default, the width-to-height ratio will be locked to prevent distortions; click the padlock icon to unlock this if needed.  

#### Adding an outline around an object
- **Option 1**: Use a simple geometric shape from the shape library.
- **Option 2**: Use the outline tool. Click “outline” and enter your desired offset.
  - If your design contains text or a lot of small details, make sure this margin is large enough to keep them all connected.
- **Option 3**: Draw your own outline using vector lines.

#### Reflecting an object
1. Select all elements of your design (object and cut outline).
2. Choose “reflect” from the top menu.
  - You can reflect horizontally or vertically.

#### Adding slots to a digital design
**Remember to resize designs *before* adding slots, since its important to preserve their precise dimensions.**
1. **Draw a slot**.
 - Use the rectangle tool to draw a rectangle.
 - Resize the rectangle to match the desired width of your slots.
 - Duplicate this, and work with the copy going forward. Leave the original alone to use as a tool for future copying & pasting.
2. **Position slots**.
 - Drag your dimensioned rectangle to position it relative to the shapes composing your invention kit.
 - Place it as desired, using the alignment tools to help with positioning. It's okay to have some overhang; you'll get rid of that shortly.
 - Note: If you need to adjust the length of your rectangle, click the padlock icon to unlock the dimensions (unconstraining width from height) in order to do so. This prevents the width from scaling proportionately.
   - Repeat as appropriate to place all slots around your shape.
3. **Eliminate overhang and join slot to shape**.
  - Select both your shape and slot(s).
  - Select "combine" -> "subtract".


### Useful Knowledge

#### Determining kerf
Unlike scissors, laser cutters work by vaporizing a tiny portion of the material they’re cutting. The *kerf* is the width of material removed.

Sometimes, when we’re creating something purely decorative, kerf can be ignored. However, when precision matters, so does kerf.

For these construction kits, precision matters. If you don’t compensate for the extra material lost to kerf, your slots will be slightly too wide to hug the cardboard tightly, causing your assemblies to be loose.

1. Cut a small shape out of your cardboard. This shape can be any size and shape, but we recommend keeping things simple with a 25-millimeter square.
2. Use calipers to measure the cutout piece.
3. Subtract this measurement from the intended size in the original design file, or from the size of the hole left behind in the stock material.
4. Subtract the shape's width from that of the hole it left behind.

       **Kerf = [width of square as designed] - [width of real square]**

**Note that since kerf can vary based on material type, material thickness, and laser cutter settings--even on the same machine--you should always do a test cut for any new project.**

#### Designing press-fit assemblies
Just like LEGOs, press-fit assemblies kits rely on friction to stay together. Their slots and tabs must be perfectly designed to achieve a tight fit.

In order to engineer press-fit systems correctly, you need to know two values:
  - **material thickness**
  - **laser kerf**

*Kerf refers to the width of the material burned away by the laser.*

**Note: although press-fit assemblies will work in any material if the fit is calibrated carefully enough, they are especially well-suited for corrugated cardboard because it is easily compressible. This allows for some give to allow a tight fit without getting stuck.**


##### 1. Use calipers to measure your material's thickness
Calipers are a useful tool for precisely measuring material thickness.

1. **Power on**: Press the power button to turn the calipers on.
2. **Zero**: Ensuring the calipers are completely closed, press the zero button. This will reset the measurement to 0.00 mm/inch.
3. **Open calipers**: Gently open the jaws of the calipers slightly wider than the thickness of the cardboard.
4. **Insert cardboard**: Place the cardboard between the jaws of the calipers, making sure it is perpendicular to the jaws for an accurate measurement.
    - Choose an edge or area of the cardboard that is free from creases or damage for the most accurate measurement.
5. **Close calipers**: Slowly slide the jaws together until they touch both sides of the cardboard. (Try not to compress the cardboard; this will lead to inaccurate readings.)
6. **Read measurement**: Read the measurement on the digital display. This will show the thickness of the cardboard in either millimeters (mm) or inches (in).
    - Use the "units" button to switch between millimeters and inches.
7. **Double check**: For accuracy, repeat several times at different points on the cardboard and average the readings.
8. **Write down measurement**: You will need this value in the next step.
- **Tips for ensuring accurate measurements**:
  - **Avoid excess pressure**: Do not apply excessive force when closing the jaws as it can compress the cardboard and result in an inaccurate measurement.
  -  **Clean jaws**: Ensure the caliper jaws are clean and free from debris before measuring.
  -  **Consistent positioning**: Measure at consistent positions to avoid variations in thickness due to structural inconsistencies in the cardboard.

##### 2. Determine kerf
Unlike scissors, laser cutters work by vaporizing a tiny portion of the material they’re cutting. The *kerf* is the width of material removed.

Sometimes, when we’re creating something purely decorative, kerf can be ignored. However, when precision matters, so does kerf.

For press-fit systems, precision matters. If you don’t compensate for the extra material lost to kerf, your slots will be slightly too wide to hug the cardboard tightly, causing your assemblies to be loose.

The only way to determine kerf is to **run a test cut and measure it**.

1. Cut a small shape out of your cardboard. This shape can be any size and shape, but we recommend keeping things simple with a 25-millimeter square.
2. Use calipers to measure the cutout piece.
3. Subtract this measurement from the intended size in the original design file, or from the size of the hole left behind in the stock material.
4. Subtract the shape's width from that of the hole it left behind.

       Kerf = [width of square as designed] - [width of real square]

**Note that kerf can vary based on material type, material thickness, and laser cutter settings--even on the same machine. You should always do a test cut for any new project.**

##### 3. Calculate slot width, run a test cut, and measure
The optimal width for the slots in a press-fit assembly is determined by two factors: 1) the width of your material, and 2) the kerf.

1. **Calculate slot width**: subtract kerf from material thickness to estimate your slot width:

       **Slot width = [measured material thickness] - [kerf]**

2. **Test to double check**. Calculations are a great place to start, but it's always best to cut some test pieces to confirm the best fit (and double check your calculations). Since some materials are more compressible than others—meaning they have some spring to them, like cardboard—the ideal slot thickness can still vary.
  - Option #1: Design your own test pieces to cut and measure. (*A comb-like design allows you to cut multiple slots of different widths into the same tester. Consider also engraving the slot's widths directly onto the comb for easy reference.*)
  - Option #2: Adjust or use the comb provided.

Cut and experiment with your tester to identify the best fit. This will be your slot width.
