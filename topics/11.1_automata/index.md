---
layout: default
title: 11. Automata Activity Steps
nav_order: 26
mathjax: true
---


# Fabricate a Custom Automaton
By Claire Dorsett, with inspiration from The Exploratorium's "Cardboard Automata" activity
{: .no_toc}


## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview
Integrate art and science: engineer movement to tell a story.

Automatons are kinetic sculptures built from simple machines that combine to create prescribed patterns of motion. Design and build your own!

Assemble a laser- or vinyl-cut box frame, and add cams and followers to engineer a desired movement. Then, craft a topper that harnesses that movement to tell a story or illustrate a scene.


### Context
From the Greek word automaton, which means “self-moving,” automata are kinetic sculptures that exhibit movement through mechanical means. They lie at the intersection of art and engineering, requiring both creative vision and a working knowledge of mechanical principles to construct.

Historically, automata were considered almost magic, moving in highly complex ways driven by—sometimes hidden!—hand-cranked gears and complex systems of simple machines.

Today, technology enables remote control through motor-powered systems.

{:toc}

## Materials
- computer with CAD software
- laser cutter
- 3D printer
- 3-5mm plywood *-or-* sturdy cardboard (box frame)
- cardboard (cams & followers)
- laser cutter
- PLA filament in a color of your choice

## Activity Steps
For this particular activity, it can be useful to start with an **experimentation** step using our pre-supplied tinkering files.

### 1. Experiment
Engineers often build early prototypes in order to explore their ideas in hands-on ways. Tinkering is a useful way to understand complex systems.
1. Assemble your box frame. Make sure the holes for your driveshaft are aligned. Use glue as needed to hold the panels together.
2. Press-fit your follower onto the follower rod. (Use glue as needed to keep it in place). Flip your frame upside-down and thread the follower rod through the hole in the top panel.
3. Thread your driveshaft halfway through the hole in one side of your frame. Add any cams in the appropriate orientation to achieve your desired motion. Then thread the driveshaft through the hole in the other side of the frame. 4. Add a crank to one side and a press-fit stopper (or blob of clay) to the other, to keep it from popping out.
5. Flip your frame back over, right-side-up. Place the follower atop the appropriate cams, adjusting the cams’ placement on the driveshaft as necessary for alignment.
6. Give your crank a spin! Does your follower shaft move as expected? (Adding a small post-it note “flag” to the top of the shaft can help make spins, twists, and turns more easily visible).
7. Experiment with different cam shapes, combinations, and positions to achieve different types of motion.
8. Document your experiments, noting any observations and findings related to gear ratios, cam profiles, and follower placements.

### 2. Ideate
1. Now that you’ve seen what’s possible, brainstorm the movement you want to create. Consider characters, actions, and scenes that could come to life through movement to help you tell a story.
2. Sketch your scene. Diagram its movements, and label the components (frame, crank, drive shaft, cams, followers) and mechanisms needed to achieve those movements (shape of cams, gears, etc.).
3. Consider how you can apply your findings to your automata design to achieve your desired movement and storytelling.

### 3. prototype
1. Use cardboard cutouts and sketches to rough out your scene. Take this opportunity to figure out sizing for your objects, and to determine which fabrication method may work best for each.
  - Consider foreground, middle-ground, and background.
  - How can you make something appear to pop out from behind something? Dive into water? Walk across a surface?

### 4. Design
1. Create CAD or vector designs for the elements that will bring your scene to life.
  - Consider how they will attach to the top of your frame, to your followers, and (if relevant) to each other.
- Note that you may choose to re-fabricate elements of your box frame depending on the design of your topper.
  - For example, if you would prefer to avoid adhesives or want to be able to swap different elements of your scene back and forth, you might laser cut a new top panel to enable tab-in-slot assembly.

### 5. Prepare files
1. Save your design files in the appropriate format for your fabrication method.
2. Load them into the software appropriate for your machines and prepare them as needed.
  - 3D printer: STL files, sliced in Bambu Labs
  - Laser cutter: SVG files with power settings calibrated in XCS
  - Vinyl cutter: SVG files with power settings calibrated in Silhouette Studio.

### 6. Fabricate
1. Load your files into the machines and launch the jobs.

### 7. Assemble
1. With your pieces fabricated, it’s time to assemble your automata!
  - Make sure your followers are properly aligned with your cams or gears.
2. Give your assembled piece a spin.
  - Does it behave as you expected?
  - Are you able to achieve your desired motion?
