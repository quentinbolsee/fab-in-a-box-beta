---
layout: default
title: 1. Vinyl-Cut Stickers Activity Steps
nav_order: 5
mathjax: true
---


# Design and Cut a Custom Vinyl Sticker
By Claire Dorsett and Racheal Naoum
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Design and fabricate your very own custom stickers!

Hand-draw or digitally design a sticker before formatting it for cutting.

This activity offers a fun introduction to 2D design and the vinyl cutter, a versatile fabrication machine.


### Context

Vinyl cutting appears in projects big and small, ranging from custom decals for laptops and water bottles to whole-body car wraps and full-wall murals. It can also be used to make custom stencils and labels.

It is a type of subtractive fabrication, meaning we start with a full sheet or roll of vinyl and remove—or subtract—the parts we don’t need.

{:toc}

## Materials
- Silhouette Studio software
- Vinyl cutter
- Vinyl sheets or roll in the color(s) of your choice
Transfer tape or transfer paper
Weeding tools: picks and tweezers
Squeegee or plastic card




## Activity Steps
There are many ways to design your sticker.

**Option #1**: Scan a hand-drawn image and clean it up in software. (Cell phone photos work just fine, too.)
- If you are comfortable with xTool's XCS software, you can follow the process outlined in the [stamp activity](topics/5.2_hand_drawn_stamp) to clean up a sketch and save it as an SVG.

**Option #2**: Use a pre-designed image sourced from the web.
- Be mindful of copyright.

**Option #3**: Design or edit your image directly in Silhouette Studio.
- The rest of this tutorial will focus on this option.


Regardless of which method or software you choose, **save your finished design as an SVG**. This is a vector format ideal for vinyl and laser cutting. (*Vectors are high-quality and maintain their clarity--preventing pixelation--even when scaled.*)

**Note: You do not need to mirror your designs.**
There is an intermediary step where we transfer the vinyl onto transfer tape before sticking it to a desired surface, meaning the finished product will come out just as it appears in the original design.


### 1. Create your design!
You can either work from scratch within Silhouette Studio, or import an existing .png or .svg file from another software or the web (respecting copyright restrictions).
- **To add text**: Choose the “text” tool on the lefthand menu, use your cursor to select your placement, and type your desired text. A toolbar will appear across the top of the screen where you can select your desired font style and size. Bolder, blockier fonts work best for pop-ups.
- **To import a file**: Drag and drop, then resize as needed.

### 2. Add an outline
This will be the outer shape of your finished sticker.
- Create a bounding box in the shape of your choice, sized to how large you want your finished piece.
- Center this around your pop-up design.
- Select your outline along with all elements of your design. Right click and select "group." This will allow you to manupilate them as a single object.

### 3. Configure cut settings
Stickers are easy: you're just cutting!
1. Select your sticker.
2. Navigate to "send."

### 4. Set up the vinyl cutter
Be sure to set up with enough room for the spool holder if you're using it. If you're using sheet vinyl, there's no need for a cutting mat.
1. Turn the machine on: long-press the power button on its right side for 2-3 seconds.
2. Connect to the machine via bluetooth or a USB cable.
3. Select the appropriate machine from the menu once it appears.
  - (Note: if a firmware update is required, you will need to install this before proceeding.)
4. Install autoblade (skip this step if already installed):
  - Open the machine’s hood.
  - On the tool carriage, pull the locking mechanism completely out.
  - Place the autoblade into the tool slot, and make sure it is fully inserted.
  - Push the locking mechanism back into place.

### 5. Prepare your material
Good news: there’s no need to use a cutting mat with vinyl!
- If using individual vinyl sheets, press the forward arrow button while feeding one end of the material into the machine, making sure it is aligned under both rollers.
- If working from a roll:
  - Set the spool holder several inches front of the machine.
  - Load the vinyl roll onto the spool holder, with its loose end on the top facing away from you and toward the machine.
  - Press the forward arrow button while feeding the end of the roll into the machine, making sure it is aligned under both rollers.

### 6. Configure cut settings
Now, it's time to prepare the file for the printer.
1. Navigate to “send” in the main menu (upper right corner of the screen). It should automatically open to "simple."
3. Choose your material from the dropdown menu.
4. Select an action: cut.

### 7. Cut!
Almost done!
1. With your mat lined up and ready, click "send file."
  - The machine will take a few moments to process the job.
2. When the machine has finished, press the unload arrow to release the vinyl while slowly pulling it out of the machine.
  - Note: Vinyl cutters only cut; they don’t remove unneeded material. That means your finished pieces may *look* like solid vinyl at first glance. Hold it at an angle to the light to see the cut lines more easily.
3. If multiple designs were printed together, use scissors or a blade and cutting mat to separate them.


### 8. Weed:
This is many people's least favorite step, as it requires a lot of patience.
1. Under good lighting, use picks, tweezers, or your fingernails to carefully peel away all vinyl that is **not** part of your finished design. This can be discarded.
  - It can help to gently hold pieces you want to keep in place as you remove elements beside them; sometimes, two or more sections will come off together. This is particularly true for tiny details, like punctuation marks.

### 9. Transfer:
Once your design has been weeded, it’s ready to be transferred to its new home!
1. Cut a piece of transfer tape or paper to a size appropriate for your design.
2. Peel the bottom (opaque) layer off of the tape and apply the sticky layer carefully over your design, avoiding any wrinkles or bubbles.
  - A plastic card (like a hotel key or student ID) can be a useful tool to help press the transfer tape down, making sure it sticks firmly to the vinyl.
  - Note: don’t remove the tape until you’re ready to stick your design to its final resting place! If you need to transport it, do so with the transfer tape still affixed to it.
3. Once you’re ready to apply your sticker to a surface, carefully remove the back layer from the vinyl. You will be left with just the vinyl itself, stuck to the transfer tape.
4. Line up your sticker where you want it, and gently press it into place. Like before, it can help to gently scrape over it with a plastic card to make sure adhesion is strong.
5. Gently peel off the transfer tape, and admire your new sticker!
