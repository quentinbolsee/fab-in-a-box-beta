---
layout: default
title: 5b. Laser-Cut Rubber Stamps (Hand-Drawn) Activity Steps
nav_order: 14
mathjax: true
---


# Make Your Mark: Turn a Hand-Drawn Design into a Rubber Stamp
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Create a custom, reusable stamp you can use to personalize your future creations!

No need to learn complicated CAD for this activity. We’ll go from a hand-drawn design to a digital file with minimal prep using just a phone camera or scanner.

### Context

From McDonalds’ golden arches to Nike’s signature swoosh, many of us can recognize hundreds of logos at a glance. This is far from accidental; these symbols were designed to be iconic.

Marketing leverages the science of psychology to influence our emotions and behaviors by combining carefully selected color schemes, fonts, and other graphics to convey a message. Logos must also work in monochrome--black and white or a single color--for contexts like screen printing and stamping.

A well-crafted logo can do more than look pretty! It can tell a story about a brand’s identity, values, and mission.


{:toc}

## Activity Steps

### 1. Research
- What eye-catching logos can you think of? Make a list.
- What makes the logos on your list memorable? What do they signify to you?
- What design elements can you identify? How do these combine to achieve impact?

  - Shape
  - Typography
  - Contrast
  - Composition

### 2. Ideate
  - Consider what makes you unique. What qualities or values do you hope to convey?

  - It can help to generate a list of words or phrases that describe you or your brand. What design elements might help you achieve this?

### 3. Analog Design: Sketching!
  - Start by sketching a simple design. This might be a variation of your name, signature, or star sign. It might be an emoji, or a logo for your personal brand. Look for inspiration in things that matter to you, like songs, books, or nature. Less is more here; you don’t need delicate detail.

### 4. Test & Iterate (optional)
  - Share your draft or design ideas with a friend.

    - What emotions or concepts does your logo evoke for others? Is your messaging coming across clearly?
    - How might you incorporate this feedback to enhance your design?

  - Taking constructive feedback into account, redesign a final draft of your logo.

### 5. Send Your Sketch to the Computer
  - Scan your final design or snap a quick photo from directly above it.
    - Cell phone cameras work fine for this!
  - Send the file to your computer.
    - USB, email, or airdrop are all fine options.

### 6. Format Image for Laser Cutting (XCS)

#### 6.1 Open laser cutter software:

  - xTool’s free Creative Space (XCS software) is downloadable here.

#### 6.2 Select material

  - Materials appearing in the drop-down menu will automatically populate xTool’s recommended settings for score/engrave/cut processes. However, if you’re using laser-safe stamp rubber, you will likely need to select “user-defined material.” That’s all for now!  We will measure the distance during a later step when we prepare the machine itself.

#### 6.3 Import image
- Click on “image” in the upper lefthand corner and import your design.
   - This works best with an SVG, but you can use any image you want, including jpg or png.
   - Note: If your image is too large, select “yes, scale to fit on canvas.” We’ll be cropping and resizing anyway.

#### 6.4 Crop away unneeded area
- Click “edit image.”
- Click the “crop” icon and draw a box around the area you want to keep. You can adjust this box after you place it, so don’t worry about being too precise to start with.  
- Important: when you’re satisfied, **click the small checkmark** at the bottom of the screen.
- Click “save.”

#### 6.5 Convert to black-and-white
- You need to do this even if you used black marker on a white background, as there is likely to be some residual color from the scanning process.

  - Select “black-and-white” from the bitmap image menu.
  - Adjust the sharpness as desired, and slide the “greyscale” slider all the way to the right (the maximum setting).

#### 6.6 Trace image to get its outline
- Select your design. In the “bitmap image” menu, click the bottom button: “trace.”
- Adjust the sliders until you’re satisfied.
- Click “save.” (It’s usually fine to use the preset settings here.) This creates an outline of your design on a new layer. **In order to see this outline, you will need to turn off visibility in the original image layer, which we will do in the next step.**

#### 6.7 Ignore and hide the original image
- In the “layer” menu, select Layer 1. This is your original image.
- Under the “object setting” menu, click "ignore."
- Turn off visibility for Layer 1. You should only see your traced outline remaining.
- (Note: you could just delete this image or the layer it’s on instead of hiding it, but you will not be able to go back and edit it later if desired.)

#### 6.8 Resize object(s)
  - Select all elements of your design and resize by entering a value in millimeters into the “size” box. You can adjust the width (W) or height (H).
  - Note: by default, the width-to-height ratio will be locked to prevent distortions; click the padlock icon to unlock this if needed.

#### 6.9 Add a bounding box
- To avoid wasting material, we want to make the outer margin as small as possible. There are a few ways to do this. Which is best for you will depend on your personal preferences and the geometry of your design. You want a nice, blocky outline that avoids narrow spots (or "bridges" between design elements), since these are fragile and might break.
  - **Option 1**: Use a simple geometric shape from the shape library.
  - **Option 2**: Use the outline tool. Click “outline” and enter your desired offset. If your design has text or a lot of small details, make sure this margin is large enough to keep them all connected.
  - **Option 3**: Draw your own outline using vector lines.

#### 6.10 Format for engraving
  - Select both the object and bounding box.
  - Select combine > subtract at overlap. The design and bounding box will now be joined into a single object.
  -With this selected, choose “engrave” as your processing type in the object setting menu.
  - Enter manual settings:
    - **Power**: 80
    - **Speed**: 80
    - **Pass**: 2
    - **Lines per cm**: 100-120
  - You may want to run some tests to dial this in for your machine and use case, but we’ve found these work well for stamps roughly three - five centimeters in size.

#### 6.11 Add cut outline
  - We don’t just want to engrave our stamp designs - we also want to cut them out! To do this, we’ll need to add another line, this time right on top of or just slightly smaller than the design’s outer perimeter. We will use the "outline" tool for this.

    1. Select the object.
    2. Uncheck “add inner line for bitmap.”
    3. Input a small **negative** offset distance: -0.1mm
    4. Move this new outline to a new layer.
    5. With the outline selected (which it should be if you haven’t clicked elsewhere yet), click “move to” in the layers menu and choose a new color.

#### 6.12 Format for cutting
  - Choose your outline layer from the layers menu.
  - Under processing type, choose “cut.” Enter manual settings:
    - **Power**: 100
    - **Speed**: 20
    - **Pass**: 3

#### 6.13 Reflect image
  - This is arguably your most important step! Stamps create mirrored images of the designs on their faces. That means you need to engrave your original design backwards onto the rubber.
   1. Select all elements of your design (object and cut outline).
   2. Choose “reflect” > “reflect horizontally” from the top menu.
  - All done! You’re ready to cut! Don’t forget to save your file for future use.

### 7. Prepare Laser Cutter

#### 7.1 Connect laser cutter
  - Turn on the laser cutter and connect it to the computer via USB.
  - Click “connect device” and select the appropriate machine.

#### 7.2 Load stock material
  - Open laser cutter lid and place stock onto honeycomb.
  - Manually drag laser head over center of stock.
  - Close lid.

#### 7.3 Auto-focus
  - With laser head centered over the stock, click the auto-focus button (this looks like a crosshair symbol next to the distance box).
  - Wait for machine to focus.
  - Open lid.
  - Manually drag laser head to desired starting position on stock. The red cross will act as a reference point, also appearing in the XCS software.

#### 7.4 Check framing
  - In the XCS software, position your design files as desired relative to the red cross (you might want the cross to land on the upper lefthand corner of your design, for example).
  - Select “framing.”
  - When instructed, press the button on the machine. (*This can be done with the machine lid open - there is no laser running during framing.*) The laser head will move to map the outer perimeter of the design’s processing area. If this does not fit on the stock or overlaps a previous cut, manually adjust the stock or digitally adjust the design’s position.

### 8. Laser Cut Stamp Face

#### 8.1 Run the job
  - Click “process” in XCS, followed by the button on the machine when instructed to do so.
  - **Important: Never leave the machine unattended while it is running!**

#### 8.2 Remove finished pieces
  - Gently check to make sure all pieces cut through. If they haven’t quite released, you can close the lid and run another cut pass. (Toggle the layer with the engraved design to “ignore” first.)
  - Remove workpieces and scrap stock from machine bed.
  - Close lid.
#### 8.3 Post-process rubber pieces
   - Use a damp cloth, sponge, or toothbrush to gently rub away soot.

### 9. Design Wooden Handle

#### 9.1 Configure material settings
  - The simplest handles are simple engravings on flat stock. 3mm plywood works nicely. Choose your material from the drop-down menu.

#### 9.2 Turn off your original cut outline
  - Toggle its layer to “ignore” under object settings.

#### 9.3 Create a new border around your original design
  - Geometric shapes are easy and elegant, but you can also apply an outline slightly larger than the original design if you prefer.
  - Move this new outline to a new layer.

#### 9.4 Apply cut settings to border
  - With the border layer selected, toggle processing type to “cut.” If you’ve chosen your stock material from the drop-down menu, the appropriate power and speed settings should automatically populate.

#### 9.5 Format design treatment
  - Select the original design and toggle processing type to either “engrave” or “score.” This is a matter of personal preference. (Scoring takes much less machine time, if this is a factor.)

#### 9.6 Reflect (again)
  - This time, you want to make sure your design is facing the correct way. Since we reflected it previously, you’ll now want to reflect it again to reverse that.

#### 9.7 Run job
  - Place handle stock onto machine bed.
  - Auto-focus.
  - Frame as needed.
  - Process.
  - Remove finished piece and scrap stock from bed.

### 10. Assemble
- Center your rubber stamp on the bottom of the block and glue in place. Ensure that “up” is the same direction on both the face and handle. Wait for the glue to dry, and you’re ready to go!

### 11. Test and Evaluate
- Get to stamping! Try several different colors of ink. How does the ink color affect how your logo is perceived by others?

### 12. Reflect & Extend
- How might you use this logo and your brand identity to develop brand recognition? What kinds of marketing might you do?
- Consider ethical questions relevant to branding and logo design, such as cultural appropriation, and misrepresentation.
- How does corporate branding influence people’s societal values?
