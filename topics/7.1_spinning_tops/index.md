---
layout: default
title: 7. Spinning Tops Activity Steps
nav_order: 18
mathjax: true
---


# 3D Print a Spinning Top
{: .no_toc}

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Give engineering a spin: experiment with physics while designing and fabricating a custom spinning top!

Get hands-on with energy, force, and momentum.

How long can your top spin before falling?

### Context
As long as it’s spinning, a top can remain upright and stable--but eventually, it will begin to wobble and shake. As it slows, it will eventually fall.

This is due to a delicate balance of forces: **angular momentum** works to keep a top spinning, while **gravity** and **friction** work to destabilize it.

#### Forces, forces everywhere!
Several key physics concepts affecting a top’s performance:

At the most basic level, tops convert **linear motion** (*movement along a straight line*) into **rotational motion** (*movement around an axis*).

- **Rotational motion**: An object’s movement around a central axis. In this case: the top’s spinning motion around its central spindle.
- **Angular Momentum**: The rotational motion of an object around a fixed axis. In this context, it refers to the measure of the top's tendency to continue rotating about its central axis.
- **Gravity**: A downward force pulling objects toward the ground. If a top spins quickly enough, it can generate enough angular momentum to counteract gravity’s force and remain upright.
- **Friction**: The resistance one objects experiences when rubbing over or against another. Tops experience friction between their points and the surfaces upon which they spin.
- **Precession**: A phenomenon where the axis of rotation of a spinning object gradually changes direction in response to an external torque. In the case of a spinning top, precession occurs as a result of gravity. This causes the top to "wobble" slightly as it spins and slows, eventually throwing it off balance.

#### In the Real World
From engines to whirlpools, spinning is ubiquitous in the real world. Let's look at two examples:

- **Figure Skating**: When a figure skater performs a spin, they draw their arms close to their chest to increase their rotational speed, much like how a spinning top's angular momentum increases when you give it a quick twist. By pulling their arms in, the skater reduces their moment of inertia, which is a measure of how mass is distributed around an axis of rotation. This reduction in moment of inertia allows them to spin faster while conserving angular momentum, just like how a spinning top maintains its rotation despite external forces.

- **The International Space Station (ISS)**: The ISS, like a spinning top, utilizes the principles of angular momentum to maintain its orientation and stability in space. Much like how a spinning top experiences precession due to external forces, the ISS occasionally experiences outside gravitational forces, (as well as interference from solar radiation pressure and atmospheric drag), and requires adjustments to its orientation to counteract.

In both figure skating and space, **conservation of angular momentum** plays a crucial role in helping achieve and maintain a powerful spin. By manipulating their moments of inertia and making precise adjustments, individuals and spacecraft alike can maintain stability in their respective environments.


{:toc}

## Materials
- Computer with CAD software
- 3D printer
- PLA filament in a color of your choice

## Activity Steps

### 1. Ideate
#### 1.1 Sketch some ideas in three dimensions
  1. Label the **features** you think will help your top perform well (brim width, spindle length, etc.)
  2. Label approximate **dimensions** for each of these features.

#### 1.2 Translate your designs into profiles
  1. First, draw a dotted line to represent your z-axis.
  2. Then, sketch a single line representing the half-profile view of your design.
    - You may find it easier to draw the whole silhouette rather than just half. That’s fine! Just make sure it doesn’t intersect with the dotted axis line more than twice: once at the top and once at the bottom. Anything more than that will create multiple bodies.
  3. Pay attention to the shape of your spindle’s **bottom point**.
    - If your point is too sharp, it may be difficult for the top to balance with any stability; if it's too flat, it may create too much friction against the surface. Small, slightly rounded points work best.
  4. Consider making your design **hollow**.
    - This keeps the mass toward the outer walls, increasing angular momentum to help your top spin longer.

### 2. Design
You can use any CAD or 3D design software you like for these steps. While specifics will depend on which software you choose, the steps outlined below offer a high-level overview of the design process.

#### 2.1 Draw your top's profile
1. Create a new front-view sketch.
2. Using the line or spline tools, sketch the shape of the half-profile your top, using the z-axis as your center line. Don’t close the sketch; leave the z-axis open.
3. To create a nicely rounded point at the bottom of your spindle, you may want to consider adding a tiny circle using the “circle” tool in CAD, and erasing any extra line segments.
  - It can be tricky getting the placement right; make sure the center of your circle lands on the z-axis.

#### 2.2 Revolve around the z-axis
1. Select the “revolve” tool in your CAD software.
2. Select the profile line you just drew as your tool, and the z-axis as your axis. Click okay.
This should have created a three-dimensional version of your drawing! Congratulations; you’ve just designed a top!

#### 2.3 Save your file
1. Save your finished top as a .stl file.
  - This is the file format most commonly used for 3D printing. Often referred to as a “mesh,” it is an intricate three-dimensional web made up of thousands upon thousands of tiny triangles. (*STL stands for stereolithography, but you can think of it as “standard triangle language” or “standard tessellation language” instead.*)

### 3. Prepare & slice files
1. Open your slicing software: Bambu Studio
  - *Note: Slicing softwares, often called “slicers,” are used to prepare .stl files for 3D printing. They offer tools and workflows to help you lay out multiple bodies on a single print bed, add supports, and more.*
2. Import your design into the slicer: drag and drop!
3. Select the type of printer you’re using (P1S).
4. Select the bed type.
5. Select the filament type being used (PLA).
6. Select the slicing settings.
7. Click “slice.” This will create a .3mf file and take you to a preview window that shows you what your finished design looks. Your dice is now ready to print!

### 4. Launch print
  You have two options to launch your print: 1) send it wirelessly, or 2) us an SD card.
1. Load filament (skip this step if already loaded):
    - Hang filament spool on holder on back of machine so that filament unwinds clockwise.
2. Insert filament into the PTFE tupe on the top back side of the machine.
3. On the machine’s screen, select nozzle → – → bed heat, and head the nozzle to the recommended temperature for the filament (for PLA).
4. Wait for the nozzle to heat up.
5. On the machine’s screen, select nozzle → E → downward arrow several times until the filament comes out of the nozzle.
6. Load file: Select the file icon and select your file to start the print.
- Note: The printer will likely run an automatic leveling check before printing. This usually takes a few minutes.

### 5. Retrieve finished tops
  Once the printer is done, pop your dice off the bed.
  - If they seem stuck, you can either: using a soft prying tool (a 3D printed one works well!), or remove the magnetic bed entirely and gently flex it to help the objects release.

### 6. Give ‘em a spin!
Give your finished tops a test spin! You may have more luck spinning them just a few millimeters above your test surface and dropping them.
  - Try them out on different surfaces. How do different textures affect their performance? Why?
