---
layout: default
title: 3. Pinwheels Activity Steps
nav_order: 9
mathjax: true
---


# Engineer a Paper Pinwheel
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Design and cut a custom paper pinwheel to experiment with energy, forces, and motion.  

### Context

P

{:toc}

## Materials
- Silhouette Studio software
- Vinyl cutter
- Light hold (low-adhesion)cutting mat
- Cardstock (preferably 100lb or higher)
- Wooden pencils with rubber erasers
- Push pins
- Circular stickers (~1 inch diameter), tape, or glue
- Fan or blowdrier with "cool" option
- (Optional) Thin but rigid laser-safe plastic sheets



## Activity Steps


### 1. Prepare a New Project in Silhouette Studio
First, you need to set up your digital workspace.
1. Open a new project in Silhouette Studio (downloadable here).
2. Input the size of the material you will be working with.
  - Under “page setup,” either select from the dropdown menu under “media size” or manually input the width and height in the text boxes below.
  - The other settings should be fine as-is, but feel free to adjust the media color or rotate the view as desired.

### 2. Create your design!
Pinwheel designs can vary significantly to include: different blade numbers, shapes, and angles; perforations in blades; and single- or multi-layered designs. Consider factors like which way you want your pinwheel to spin and engineer your design accordingly. (Can you create a two-layer design where the layers spin in opposite directions?)

#### 2.1 Draw an outer shape / bounding box
The outer cut shape, this bounding box will determine how your blades are shaped.
- Start with shapes with *radial symmetry* (circles, squares, regular polygons) before leveling up to experiment with radially asymmetrical or irregular shapes (ellipses, rectangles, irregular polygons).
  - Simple, traditional pinwheels are engineered from square templates.

#### 2.2 Find the center
This is where your pivot point will attach to your pinwheel's handle (where your pushpin will secure the pinwheel to the wooden chopstick or dowel).
- Drop a point or a tiny circle at the center of your design.

#### 2.3 Add cut lines
Using the line tool, use cut lines to split your pinwheel into any number of equal segments.
- Think of this like cutting a pizza or a pie; each line should go all the way across your shape from one side to the other, intersecting with your center point along the way.
- When cut, these segments will create "petals." Each petal will fold into one blade.
- Design tips:
  - Start with straight lines before leveling up to experiment with curved lines.
  -  What happens if your segments aren't equal? If time allows, experiment to find out!

#### 2.4 Create a center margin
This ensures you have a margin toward which to fold and onto which to pin your blades.
1. Draw a small circle around your center point, within the confines of your larger shape. This should have a radius of at least 1/2 inch.
2. Erase the cut lines within this smaller, inner shape.
3. Erase or ignore the inner shape; you do not want to cut or perforate around its perimeter.

#### 2.5 Add perforations, cutouts, or other decorative elements
This step is optional, for more advanced experimentation.
- Add crease lines or small, shaped cutouts as desired.
  - Remember, wind will pass directly through any perforations!


### 3. Configure cut settings
Now, it's time to prepare the file for the printer.

1. Navigate to “send” in the main menu (upper right corner of the screen).
2. Select “line” from the top menu. Each line color—-in this case, red and blue--should have its own row.
3. Choose your material for both rows (they will be the same; likely cardstock, plain although coverstock, heavy also works, depending on your material).
4. Select an action for each row: cut for red and score for blue.
 - If you are using the autoblade tool, you can perform both functions in the same pass. Choose tool no. 1 for both rows. (*Note: your scored lines will be finely perforated rather than scored.*)
 - If you are using two different tools—one for cutting and another for scoring—choose the correct tool number and carriage as appropriate for each row.


### 4. Set up the vinyl cutter
Be sure to set up somewhere with enough room for the mat to feed through. There's no need for a spool feeder for this project, since you'll be using cardstock and a mat.
1. Turn the machine on: long-press the power button on its right side for 2-3 seconds.
2. Connect to the machine via bluetooth or a USB cable.
3. Select the appropriate machine from the menu once it appears.
  - (Note: if a firmware update is required, you will need to install this before proceeding.)
4. Install autoblade (skip this step if already installed):
  - Open the machine’s hood.
  - On the tool carriage, pull the locking mechanism completely out.
  - Place the autoblade into the tool slot, and make sure it is fully inserted.
  - Push the locking mechanism back into place.


### 5. Prepare your material on a mat
We'll be using a **low-adhesion mat** for this project.
1. Position your cardstock to align the upper left corner of the material with the upper left corner of the mat and press it firmly into place.
2. Line the mat up with the machine.
3. Push the arrow button to load the mat, centering it between the pressure wheels.
4. Use the arrow buttons to align the toolhead with the upper right corner of where you wish to cut.

### 6. Cut!
Almost done!
1. With your mat lined up and ready, click "send file."
  - The machine will take a few moments to process your file, cutting first and then scoring.
2. When the machine has finished, press the unload arrow to release the cutting mat.

### 7. Remove your material from the mat.
To remove your workpiece, peel the mat off of the cardstock instead of vice versa. This helps keep your finished product flat and intact. Peeling the workpiece from the mat will likely result in torn or curled cardstock.
  - Turn the mat over so your workpiece is flat on the table. Then, gently peel the mat off of the workpiece.

### 8. Bend and crease
Bring your design into three dimensions.
1. Gently bend one corner of each petal in toward your center point, gathering and stacking them there. **Use gentle, arcing folds--not sharp creases.**
  - Use a tape, glue, or a sticker to hold these in place.
3. Gently push your pushpin through the center of the tape or sticker at the center of your pinwheel.
4. Gently use the pushpin to attach your pinwheel to a pencil eraser.
  - Don't push your pushpin all the way in; it should be secured firmly to the eraser, but your pinwheel should still be able to turn freely.

### 9. Test and refine
Use a fan or hair dryer (on the low/cool setting) to pass air over the blades of your pinwheel, aiming directly at its centerpoint. Does it spin?
- How stable is your pinwheel's motion? What constraints, like friction, seem to be constraining it, and how can these be minimized?
