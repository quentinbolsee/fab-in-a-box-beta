---
layout: default
title: 5. Laser-Cut Rubber Stamps Facilitator Guide
nav_order: 15
mathjax: true
---


# Faciltator Guide: Laser Cut a Custom Stamp
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Learners will use digital fabrication to create a custom stamp featuring a personal design or logo that reflects their unique identities and values.

By combining graphic design principles with self-reflection, they will use symbols and typography while designing their stamps, thinking critically about symmetry and composition.  

This activity offers an introduction into laser cutting, scoring, and engraving, including file prep and machine operation using both laser-safe rubber and wood.

Designs can either be **hand-drawn** and digitally optimized for the laser cutter, or designed **entirely digitally**.



### Context

From McDonalds’ golden arches to Nike’s signature swoosh, many of us can recognize hundreds of logos at a glance. This is far from accidental; these symbols were designed to be iconic.

Marketing leverages the science of psychology to influence our emotions and behaviors by combining carefully selected color schemes, fonts, and other graphics to convey a message. Logos must also work in monochrome--black and white or a single color--for contexts like screen printing and stamping.

A well-crafted logo can do more than look pretty! It can tell a story about a brand’s identity, values, and mission.

{:toc}



## Scalability
### 1. Exposure: Designing Digitally
Digitally design a custom stamp design and pair it with a laser-engraved handle.

- Introduces XCS software and digital design.
    - Designs can be size-constrained (~2-3" square)
- **Curricular tie-ins**: marketing, business, art, psychology
  - Design a logo for a brand or business idea (research popular logos, analyze the design decisions behind them through the lens of psychology, etc.)


### 2. Exploration: Optimizing Hand-Drawn Designs
Hand-draw and digitally optimize a custom stamp design for laser cutting, and pair it with a laser-engraved handle.

- Introduces optimization from hand-drawn designs to laser-ready files.
- Incorporates extra brainstorming and discussion steps re: the design process and stamp composition.
- **Curricular tie-ins**: marketing, business, art, psychology
  - Design a logo for a brand or business idea (research popular logos, analyze the design decisions behind them through the lens of psychology, etc.)


### 3. Deep Dive: Add a Press-Fit Handle *-or-* Go Deeper into Logo Design
Upgrade from a two-dimensional engraved wooden panel to a three-dimensional press-fit handle -or- dedicate multiple sessions to deeper logo research, design, and reflection.
- Introduces kerf and material measurements for press-fit optimization.
- Pitch a brand idea (or pitch a redesign for an existing brand, like Nike), and design a custom logo that expresses that brand’s unique identity and values.
  - Pair and share to gather “customer” thoughts about your logo, and refine designs based on feedback collected.
- **Curricular tie-ins**: marketing, business, art, psychology
  - Design a logo for a brand or business idea (research popular logos, analyze the design decisions behind them through the lens of psychology, etc.)

### Extension: Printmaking
Instead of (or in addition to) attaching stamps to wooden handles, lay them on their backs and use paint or markers to color in their faces. Apply a piece of paper over top and use a roller to achieve even pressure across the stampface.
  - Similar to lithographic printmaking, this process allows multiple colors to be applied at once.
  - Alternatively, learners can produce multiple stamps, each illustrating a different layer of the same scene. These can be stamped over top of each other to achieve the same effect.



## Materials
- Printable worksheets
- Laser cutter
- XCS software (and/or other two-dimensional design software)
- Black markers (washable for young learners)
- Low-odor, laser-safe rubber
- 3mm plywood for handles
- Adhesive to join rubber to wood (Power Tac, Rubber Cement, or E6000)
- Soap, water, and soft sponges or toothbrushes to wash soot off of the stamp faces post-cutting.



{:toc}


{:toc}


## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas:
    -

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Consider shape and composition when designing in 2D.
2. Identify, discuss, and apply key principles of graphic design like balance, symmetry, and negative space to tell a story.
3. Make and explain design choices based on what they wish to convey and to whom.
4. Give, receive, and incorporate constructive feedback.
5. Understand how design can impact consumer perception and brand identity.
6. Relate their learning to real-world companies and entrepreneurs.


### Fabrication goals
Learners will be able to…
1. Translate an idea into 2D design.
2. Translate a 2D design into a laser-cut file / prepare a file for laser cutting.
3. Engrave, cut, and score on the laser cutter.
5. Post-process laser-cut rubber.


## Facilitator Notes
### 1. Setup & pre-preparation
- Learners can work alone or in groups of 2-3.
- For hand-drawn logos, use our printable worksheets or design your own to keep students’ designs constrained to the proper size. (Work boxes should be scaled to the desired size for the finished stamp faces!)
  - Younger learners can design on canvases twice or three times the size of the finished stamp, if desired. Their designs can then be scaled digitally before laser cutting.
- Consider running some test cuts to determine the limits of your laser cutter on rubber.
  - Cut the word "test" in different font styles and sizes to demonstrate which work and which are too fine.

### 2. Batch processing
- To save time, learners’ designs can be engraved and cut in small batches or all at once during a break rather than one by one.
- If batching for a make-and-take where you are less worried about teaching the intricacies of the design software, you may want to configure **hand-drawn** files on the control computer all at once.

### 3. Reflecting
- Don’t forget to reflect (flip) learners’ designs before engraving!
  - Stamps create mirrored images of their faces when used. That means words and letters should appear backwards on the stamps themselves!

### 4. Tips
- If running the “design a brand logo” version of this activity, consider assigning brands or companies to each group rather than asking learners to create their own.
  - Give each one a core focus and 2–3 values. You can provide the names or ask learners to come up with their own.
  - Alternatively, consider redesigning logos for existing brands, like Nike.
    - Ask learners to list 2–3 values they associate with the company before getting started.

### 5. Material considerations
- This activity works best with laser-safe rubber, but can be replicated using wood (5mm works) or acrylic, or by gluing together cardboard layers. The key here is to ensure that the ink you're ultimately using will stick to the material.


### 5. Design considerations to share with learners
- Details that are too small (or fonts that are too narrow) may essentially disintegrate during the engraving process.
  - Bold, blocky fonts work best.
  - Consider running a test before starting your design work: engrave "test" in multiple font styles and sizes to determine which work well and which do not.
- Remember to reflect designs before cutting! Otherwise, they will appear correctly on the stamp face itself but backwards on stamped materials.
- Take advantage of the tools in your design software, like patterning, reflecting, and aligning.


## Discussion questions
- **Math (geometries, reflections, patterns)**
1. How does the concept of symmetry play a role in designing a logo?
2. What geometric shapes are most effective in creating a balanced and visually appealing logo?
3. How can reflections and patterns be used to create a unique and recognizable logo?
4. How do you calculate the area and perimeter of different shapes in your design?
5. What are the mathematical considerations when scaling your hand-drawn design to fit the stamp face size?

- **Art & Design**
1. What are the key principles of graphic design, and how do they apply to logo creation?
2. How do balance, contrast, and negative space influence the effectiveness of a logo?
3. What considerations should be made when choosing fonts for a logo?
4. How does color affect a logo, and how can this be used when designing for monochromatic (single-color) applications like stamps?
5. How does the process of receiving and incorporating feedback enhance your design?

- **English Language Arts**
1. How can a logo tell a story about a brand or individual?
2. What emotions or messages do you want your logo to convey, and how can you achieve this through design?
3. How does the use of typography in a logo influence its perception and readability?
4. What are the key elements of a successful brand story, and how can your logo contribute to this narrative?
5. How can you use descriptive language to explain and justify your design choices?

- **History & Social Studies**
1. How have logos evolved over time, and what historical events have influenced their design?
2. What are some examples of rebranding, and what prompted these changes?
3. How do cultural and societal changes impact logo design and branding strategies?
4. What ethical considerations should be taken into account when designing a logo (e.g., avoiding cultural appropriation)?
5. How does globalization affect branding and logo design for different cultural audiences?
6. How can historical and societal changes influence the evolution of brand logos?
7. What are the potential impacts of a logo on societal values and consumer behavior?

- **Engineering & Technology**
1. What safety procedures should be followed when operating a laser cutter?
2. How can different laser cutter settings (power, speed, passes) affect the outcome?
3. How have technological advancements influenced the process and style of logo design?

- **Entrepreneurship & Business**
1. How does a well-designed logo impact a brand’s identity and consumer perception?
2. hat are some iconic logos you are aware of, and what makes them effective or memorable?
3. How can a logo help in building brand recognition and loyalty?
4. Why is it important for different design elements (shape, color, typography) to work together to create a cohesive brand image?
5. What role do trademarks and copyright laws play in protecting brand logos?

- **Creativity & Problem Solving**
1. How did you come up with the initial idea for your logo?
2. What challenges did you encounter during the design and fabrication process, and how did you overcome them?
3. How did feedback from peers influence your final design?
4. What creative techniques did you use to make your logo stand out?
5. How can you apply the skills learned in this activity to other design projects or entrepreneurial ventures?
