---
layout: default
title: 7 Spinning Tops Facilitator Guide
nav_order: 19
mathjax: true
---


# Faciltator Guide: 3D Print a Spinning Top
By Claire Dorsett

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Learners will experiment with physics while designing and fabricating custom spinning tops!

This activity introduces the "extrude" and/or "revolve" commands in CAD software as well as how to use a slicer to prep files for 3D printing. It also offers a hands-on way to explore the topics of energy, Forces, and momentum while practicing iterative design in digital fabrication.

Learners will be challenged to design tops that can spin as long as possible, requiring careful engineering to achieve a balance of speed and stability.

### Context
As long as it’s spinning, a top can remain upright and stable--but eventually, it will begin to wobble and shake. As it slows, it will eventually fall.

This is due to a delicate balance of forces: **angular momentum** works to keep a top spinning, while **gravity** and **friction** work to destabilize it.

#### Forces, forces everywhere!
Several key physics concepts affecting a top’s performance:

At the most basic level, tops convert **linear motion** (*movement along a straight line*) into **rotational motion** (*movement around an axis*).

- **Rotational motion**: An object’s movement around a central axis. In this case: the top’s spinning motion around its central spindle.
- **Angular Momentum**: The rotational motion of an object around a fixed axis. In this context, it refers to the measure of the top's tendency to continue rotating about its central axis.
- **Gravity**: A downward force pulling objects toward the ground. If a top spins quickly enough, it can generate enough angular momentum to counteract gravity’s force and remain upright.
- **Friction**: The resistance one objects experiences when rubbing over or against another. Tops experience friction between their points and the surfaces upon which they spin.
- **Precession**: A phenomenon where the axis of rotation of a spinning object gradually changes direction in response to an external torque. In the case of a spinning top, precession occurs as a result of gravity. This causes the top to "wobble" slightly as it spins and slows, eventually throwing it off balance.

#### In the Real World
From engines to whirlpools, spinning is ubiquitous in the real world. Let's look at two examples:

- **Figure Skating**: When a figure skater performs a spin, they draw their arms close to their chest to increase their rotational speed, much like how a spinning top's angular momentum increases when you give it a quick twist. By pulling their arms in, the skater reduces their moment of inertia, which is a measure of how mass is distributed around an axis of rotation. This reduction in moment of inertia allows them to spin faster while conserving angular momentum, just like how a spinning top maintains its rotation despite external forces.

- **The International Space Station (ISS)**: The ISS, like a spinning top, utilizes the principles of angular momentum to maintain its orientation and stability in space. Much like how a spinning top experiences precession due to external forces, the ISS occasionally experiences outside gravitational forces, (as well as interference from solar radiation pressure and atmospheric drag), and requires adjustments to its orientation to counteract.

In both figure skating and space, **conservation of angular momentum** plays a crucial role in helping achieve and maintain a powerful spin. By manipulating their moments of inertia and making precise adjustments, individuals and spacecraft alike can maintain stability in their respective environments.


{:toc}


## Scalability
### 1. Exposure: Single-Piece Revolved Tops
Design, slice, and 3D print single-piece tops using the "revolve" command in CAD.

- Introduces the "revolve" command.
- Optional focus on hollow designs.
- Single print per learner (batch printing is an option).
- **Curricular tie-ins**: engineering, math
  - Forces, angular momentum, geometry of spindle point.

### 2. Exploration: Multi-Piece Stackable Tops
Design and fabricate multi-piece tops with disks that stack interchangeably onto a spindle to facilitate iterative experimentation.

- Introduces multi-piece design for iterative testing.
  - Square spindles with tiny support platforms and removable, stackable layers.
  - All components within each learner's set can be 3D printed at once (batch printing for multiple students at once is an option) while still allowing them to test out multiple designs via manual reconfiguration.
- **Curricular tie-ins**: engineering, math
  - Forces, angular momentum, geometry of spindle point.
  - Investigation of how different arrangements of the same layers can change the distribution of mass around a rotational axis to affect the center of gravity.
  - Can run, document, and graph the results from experiments with different designs while still only running one set of prints per learner.

### 3. Deep Dive: Mechanically Launchable or Asymmetrical Tops
Design fabricate tops that can be launched like BeyBlades *-or-* asymmetrical tops that are still able to find their balance.

- More complex CAD design work.
- **Curricular tie-ins**: engineering, math
  - Forces, angular momentum, geometry of spindle point
  - Symmetry

### Extension: Optical Illusions
  - Learners can add hand-drawn or vinyl-cut decorative elements to the tops of their tops to investigate optical illusions as they spin.
    - Color-blending
    - Movement (zoetrope-style)


{:toc}

## Materials
- Computer with CAD software
- 3D printer
- PLA filament in a color of your choice


{:toc}

## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Identify the forces acting upon a spinning top.
2. Understand how a top’s shape, weight distribution, and center of gravity affect how it spins.
3. Define angular momentum and articulate its relevance.
4. Develop and test hypotheses around how to optimize the longevity and stability of a top’s spin.
5. Evaluate their own designs.



### Fabrication goals
Learners will be able to…
1. Use the “revolve” and/or pattern functions in CAD to create three-dimensional forms around a central axis.
2. Gauge tolerances for 3D printing press-fit designs (optional).
3. Prepare & slice a file for 3D printing.
4. Add supports as needed for 3D printing.
5. Use the 3D printer to fabricate an object of their own design.




## Facilitator Notes
### 1. Setup & pre-preparation
- Print optional handouts for each learner to help with brainstorming.
- Bring examples to compare and discuss different tops or spinners before starting the brainstorming and design process: ideally, several of different designs (tall and narrow, short and squat, etc.)
  - Ask learners why they think different tops spin at different speeds; why they spin for different lengths of time; and what factors they think affect a top’s stability while spinning.
- Practice drawing profiles of three-dimensional objects.
  - Assemble a handful of irregularly shaped but rotationally symmetrical three-dimensional objects, like vases.
  - Ask learners to line-sketch front views (profiles) of these objects.
  - Ask learners to add a dotted line to their sketches to illustrate their z-axis.
  - Ask learners to trace over half of their drawings—the portion on just the right side of the vertical axis line—using a bolder penstroke or different color. This shows the design that gets revolved.
- Optional: practice paper fan-art to illustrate how revolving a shape around a central axis can result in a 3D object.
- To facilitate experimentation and design comparisons when multiple rounds of printing aren’t feasible, consider running option #2, "Exploration." In this version of the activity, learners can design and print multiple options (different shapes, for example) all at once instead of in successive rounds.
- When designing multi-piece stackable tops, the spindle must be an angled shape (like a square) when viewed top-down, with the holes in the stackable pieces matching this shape. A round spindle would simply spin freely in a round hole.

### 2. Batch processing
- Learners’ designs can be batched together for printing, with the prints launched during a reflection period or overnight. Depending on their sizes, up to 25 individual tops or roughly 6 sets of multi-piece kits can fit on a single print bed.

### 3. Time-savers
  - If 3D printing is not an option during the lesson, spindles can be pre-printed ahead of time and distributed to learners and stackable layers can be laser-cut out of acrylic or wood.  

### 4. Material considerations
- Stackable layers can also be laser cut out of acrylic or wood and stacked onto 3D printed spindles.

### 5. Design considerations to share with learners
- When designing multi-piece stackable tops, the spindle must be an angled shape (like a square) when viewed top-down, with the holes in the stackable pieces matching this shape. A round spindle would simply spin freely in a round hole.
- Center of gravity: The center of gravity plays a crucial role in determining the stability of a spinning top. For a top to spin smoothly and remain upright, its center of gravity must align with the axis of rotation. This alignment ensures that the gravitational force acting upon it balanced. A lower center of gravity can enhance stability. Engineers can design tops with features like weighted bases to help prolong spin times.
- Balanced rotation: Symmetry can help contribute to balanced rotation. A symmetrically shaped top distributes its mass evenly around its axis of rotation, reducing wobbling and vibration during spinning.

## Extension ideas
1. **Weight comparisons within a geometrically identical design**: Print two tops with different infills (or different wall thicknesses, if hollow) but identical shapes.
  - One is lighter than the other. Which will stay spinning longer?
2. **Shape/size comparison**: Design an experiment to test tops of different shapes (circle, square, triangle) or sizes (small, medium, large).
  - Make predictions about which will spin the longest, travel the farthest, or have the most stability.
  - Test out the different designs and compare the results to your predictions.
  - Why do you think the different designs perform differently?



## Discussion questions
- **General**
1. What observations did you make during your first test spin of the top, and how (if at all) did you adjust your design based on these observations?
2. What criteria did you use to evaluate the success of your spinning top design?
3. How could you design an experiment to test the effects of different shapes, sizes, or weights on the spin performance of your tops?
4. What hypotheses did you form about the factors that influence the longevity and stability of a top’s spin, and how did you test them?
5. How can documenting and graphing the results of your experiments help you understand the physics of spinning tops better?
6. Beyond ice skating and the ISS, can you think of other real-world objects or systems that rely on the principles of angular momentum and rotational motion to function effectively?
7. How might engineers use the concepts learned in this activity to solve practical problems in various fields?

- **Science: Physics & Engineering**
1. How does the shape and weight distribution of a spinning top affect its stability and spin time?
2. What is angular momentum, and why is it important for the spinning top to maintain its motion?
3. How do gravity and friction influence the performance of a spinning top?

- **Creativity & Problem Solving**
1. What challenges did you encounter during the design and fabrication process, and how did you overcome them?
2. What creative techniques or tools did you use to when designing your tops?
