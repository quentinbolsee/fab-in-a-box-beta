---
layout: default
title: 9. Casting & Molding Facilitator Guide
nav_order: 23
mathjax: true
---


# Faciltator Guide: 3D Printing for Casting & Molding
By Claire Dorsett

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}


## Overview
{: .no_toc}

Learners will design and 3D print their very own one- or two-part molds!

These can then be used to cast objects directly, or to create secondary, more flexible molds to cast large batches of a design.

This activity offers an introduction to complex, multi-step 3D design including the "cut" and "join" tools as well as an overview of casting and molding.

### Context
Casting is a manufacturing process in which a liquid is poured or injected into a rigid structure called a **mold** and allowed to cure or solidify. It takes advantage of the properties of liquids; specifically, their ability to conform to the shape of a vessel into which they’re poured.

The special liquids that are poured into molds are called **casting mediums**. These are typically two-part mixtures that undergo a thermal or chemical change called *curing* to harden into a solid, creating items like:

  - Specially-shaped bars of soap
  - Action figures and other plastic toys
  - Fancy chocolates

The individual components must be precisely measured, combined at specific ratios, and mixed together fully in order to work correctly. Otherwise, the mixture will not cure properly, resulting in a wet or soft end product.


#### In the Real World
Sand castles are often made using molds. Wet sand is packed into the forms, which are then carefully removed to leave behind castles in their shapes.

Ice cubes and popsicles are also made made from molds. Water or juice is poured into the forms, which are then frozen.

Plastic toys, clothes hangers, and utensils are also made from casting polymers into specially-designed molds.

#### Key Concepts:
There are two key types of molds relevant to this activity (and many more used in real-world manufacturing!).
1. **Single-part molds**: Simple shapes without undercuts can be cast in single-part molds.
  - Straightforward to design, produce, and use.
  - Use less material and require less production time.
  - Easy to pour and de-mold.
2. **Multi-part molds**: Some shapes are too complex to cast using a single-part mold. These require multi-part molds, in which two or more parts fit together like three-dimensional puzzles, encompassing the object to be cast. Small funnels and air vents allow a casting medium to be poured in, and air bubbles to escape. Once the casting medium has cured, the multiple pieces of the mold are disassembled to provide access to the finished part inside.
  - Allow for complex shapes, undercuts, and intricate details.
  - Provide better access to all surfaces of the object.
  - Can be designed along natural seams or edges for the object being cast to minimize visible lines on the finished casting.
Multi-part molds require special surface features called *registers* to help them lock together precisely, as well as exposed tunnels called *sprues* and *vents* to allow material to enter and air to escape.

##### Key Terms:
  - **Mold**: The rigid frame into which a casting medium is poured.
  - **Casting Medium**: The material you are pouring into a mold. This undergoes a chemical or physical change to harden from a liquid into a solid, and often gets mixed together from two parts.
  - **Cast**: To pour a casting medium into a mold; the solid form that comes out of a mold.
  - **Curing**: The chemical process a casting medium undergoes while hardening.
  - **Registers**: In multi-part molds, registers are design features that interlock to keep the mold precisely aligned during pouring and curing.
  - **Sprue**: In multi-part molds, sprues are like small, funnel-like openings into which the casting medium is poured.
  - **Vent**: In multi part molds, vents are small air shafts that allow air bubbles to escape from the interior cavity, preventing empty spaces in the finished product.

{:toc}

## Scalability
### 1. Exposure: Single-Part, Single-Step Molds
Design and 3D print a custom, single-part mold. In this version of the activity, the 3D print is the final mold and material is poured into the top.

- Introduces the "extrude" and "fillet" commands in CAD (and optionally "draft" and "fillet").
- Investigates and compares both proud (positive distance) and recessed (negative distance) extrusions.
- Plays with the surface features of dice.
- **Curricular tie-ins**: art, design, manufacturing


### 2. Exploration: Single-Part, Two-Step Molds *-or-* Two-Part, Single-Step Molds
Option A) Design and 3D print a custom **single-part, two step mold**: In this version, the 3D print is simply a tool used to create the final mold. A casting agent is poured into the print and allowed to cure. This cured product then represents the final mold, and will be more flexible than the print. (*Longer time requirement: two casting steps.*)

- Allows for experimentation with / comparisons between different casting materials and their characteristics.
- **Curricular tie-ins**: art, design, manufacturing, chemistry


Option B) Design and 3D print a custom **multi-part, single step mold**: In this version, the 3D printed parts assemble into the final mold; however, registers, sprues, and vents must be added to guide the prints' alignment and allow material to enter and air to exit. (*Shorter time requirement: one casting step.*)

- Introduces more complex spatial reasoning.
- Requires more complex CAD design.
- **Curricular tie-ins**: art, design, manufacturing


### 3. Deep Dive: Two-Part, Two-Step Molds
Design and 3D print a custom, multi-part mold. In this version the 3D printed parts assemble into the final mold; however, registers, sprues, and vents must be added to guide the prints' alignment and allow material to enter and air to exit.

- Introduces more complex spatial reasoning.
- Allows learners to experiment with different casting materials or choose a material based on the intended application of their final product.
- **Curricular tie-ins**: art, design, manufacturing


### Extension: Multi-Part (>2) Molds
Some complex geometries can't be cast from just two-part molds. They require too many undercuts or unique details. Challenge learners to create a three- or more-part mold.



{:toc}

## Materials
- Computer with CAD software
- 3D printer
- PLA in a color of your choice
- Casting mediums (any or all)
  - Playdough
  - Hydrostone
  - Oomoo
  - Wax - will also require a crucible or pan and hotplate
- Mold release agent (optional)
- Mixing tools:
  - Stirring sticks (popsicle sticks or tongue depressors)
  - Mixing cups (paper cups)
-Measuring tools:
  - Small scales
  - Disposable volumetric measuring cups
- Safety equipment:
  - Gloves
  - Eye protection
  - Lab coats or aprons
  - Close-toed shoes for all participants


{:toc}


## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Identify basic elements (tools/materials) and purposes of casting.
2. Describe the steps in the casting process.
3. Understand how molds and casting facilitate mass production in manufacturing.
4. Develop an understanding of chemical changes in casting mediums.
5. Compare the volume and/or weight of pre-cured liquid casting materials and post-cured parts.



### Fabrication goals
Learners will be able to…
1. Design in 3D.
2. Read a data sheet for casting mediums.
3. Understand the importance of gloves and eye safety.
4. Determine which casting agents to use based on the material a mold is made from, or the desired properties of the finished product (i.e. not casting sortaclear into oomoo).
5. Successfully mix two-part mixtures by weight or volume (i.e. oomoo, hydrostone).
6. Create a mold of an existing object or design one for an original idea.
7. Cast from a mold.



## Facilitator Notes
### 1. Setup & pre-preparation
- This is a messy process! Lay newspaper or butcher paper over any work surfaces to help simplify cleanup.
- Consider blocking off any nearby sinks or lining drains drains with plastic wrap to prevent any accidental discarding of casting mediums into plumbing.
- Discuss examples: Collect an assortment of plastic items that have been cast and challenge learners to look for their sprue marks, (tiny "belly buttons" that show where the casting material was poured into the mold) or seams (that show where the mold's halves would have come together).
  - You can also challenge learners to guess which type of mold/casting was used for various items (i.e. rotary molding for hollow items).
- For young learners, it can help to run a pre-activity creating custom ice-cubes or popsicles. This can expose them to the idea of a liquid being poured into a rigid form and hardening into a solid, taking on its shape.


### 2. Safety & Logistics
- It’s tempting to run an edible activity here, like molding custom candies. However, note that in order for an activity to be legally considered food-safe, EVERY STEP of its process must be food-safe. That means the 3D printer (nozzle, bed, and all parts) would need to be food-safe; the PLA would need to be replaced with a food-safe medium, etc.
- Cover safety protocols: Many curing processes are exothermic! That means the casting medium releases heat as it solidifies. This can potentially be dangerous if it occurs in a small, confined space or near something flammable. Leave excess mixed mediums alone in their cups to cure fully before disposing of them.
- Consider reactions: Some polymers react or otherwise interfere with certain casting mediums during the curing process. Read the data sheet for more details. (For example: It is fine to cast Sortaclear or Oomoo in a PLA print—but for a multi-step piece, you cannot cast Sortaclear in an Oomoo mold, or vice versa, as they will meld together during curing.)
- Discuss proper disposal procedures: DO NOT pour casting materials down the drain or into any plumbing. When mixed together with their activators (or in the case of hydrostone, with water) they will cure and solidify. This will cause extensive damage to pipes and plumbing. We advise to do this activity far away from plumbing to prevent accidents.

### 3. Batch processing
- 3D printed molds can be batch printed, but casting must happen individually for each learner. This should be done at the end of a session so that curing can happen overnight / between sessions.   

### 4. Design considerations to share with learners
- Start by modeling the thing you want to create a cast of. Then, use that model as a tool to "cut" or "join with" a bounding box, depending on whether you are creating a single- or two-step mold.
- For two-part molds, you may wish to add registers to help the pieces align more precisely.
  - Small hemispheres in diagonally opposite corners work well for this. The hemispheres on one half should be positive (convex) while those on the other should be negative (concave).
  - Consider making the concave hemispheres a tiny bit (~.5mm) larger in diameter than the convex ones to add a little buffer.

### 5. De-molding tips
- A flat tool like a paint scraper can help gently pry multi-part molds apart.
- Scissors can be used to snip off overflow from the sprue.


## Discussion questions
- **General**
1. How are casting and molding processes used in real-world manufacturing? Can you give four examples?
2. What are some advantages and disadvantages of using 3D-printed molds in comparison to those made from a more flexible material?
3. How do undercuts affect the design of a mold, and what strategies can you use to accommodate them in single-part and multi-part molds?
4. What are the advantages and disadvantages of using multi-part molds for complex geometries compared to single-part molds?
5. How do registers, sprues, and vents improve the functionality of multi-part molds?
6. What are some common issues that can arise with casting materials, such as air bubbles or incomplete curing, and how can these be prevented or resolved?
7. How does the choice of mold release agent influence the casting process and the quality of the final product?

- **Math: Geometry & Volume**
1. How does the shape of your mold affect the volume of the casting medium required?
2. How can you calculate the volume of the mold cavity to determine how much casting medium you need?
3. What geometric principles are important when designing a single-part versus a multi-part mold?
4. How do the dimensions and tolerances of your mold design affect the final cast object's accuracy?
5. What is the difference between ratios by weight versus volume? Are they interchangeable in the same proportions? Why or why not?

- **Chemistry: Properties of Casting Materials**
1. What are the properties of different casting materials (e.g., Hydrostone, Oomoo, wax) and how do these properties affect the casting process?
2. What factors influence the curing time and final properties of the cast object?
3. Why is it important to mix casting materials at specific ratios, and what can happen if these ratios are not accurately measured?
4. How do exothermic reactions during the curing process affect the choice of materials and the safety precautions needed?

- **Manufacturing**
1. How does the use of 3D printing for mold making compare to other mold-making methods in terms of speed, cost, and precision?
2. What are some applications of casting and molding in modern manufacturing, and how do they benefit from advancements in 3D printing technology?
3. How can iterative design and testing improve the efficiency and quality of the manufacturing process when using 3D-printed molds?
4. What are some environmental considerations when using different casting materials, and how can sustainable practices be integrated into the manufacturing process?

- **Design**
1. How can you determine the optimal placement of sprues and vents in a multi-part mold to ensure complete filling and minimize defects?
2. What are some techniques for creating complex internal geometries within a mold? How do these techniques influence the final cast object?
3. How can the design of a mold be optimized for repetitive use in mass production?
