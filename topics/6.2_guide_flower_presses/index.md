---
layout: default
title: 6. Botanical Presses Facilitator Guide
nav_order: 17
mathjax: true
---


# Faciltator Guide: Fabricate a Flower Press
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview
In this activity, learners will use digital fabrication tools to make an analog tool of their own: a botanical press that can be used to preserve leaves, buds, and blossoms.

Presses consist of two wooden face plates with layers of cardboard sandwiched between them, held together using wing nuts and bolts. Their outer shapes and engraved details are fully customizable.

This activity offers an introduction to 2D design, laser cutting, and laser engraving for both plywood and cardboard, along with assembling a system.


### Context
For centuries, botanists and hobbyists alike have used flower presses to preserve to document plant species, create art, and preserve botanical specimens for scientific study. During the Victorian era in particular, the practice was a popular pastime with pressed flowers used in scrapbooks, cards, and framed artwork.

Today, flower pressing continues to offer a hands-on way to explore plant anatomy, biodiversity, and ecology--or simply to capture memories.

Pressed botanicals can be used for dissection or in artwork and crafts.

{:toc}


## Scalability
### 1. Exposure: Customize a Flower Press
Digitally design parts for a custom flower press, including both engraved wooden face plates and inner cardboard layers.

- Introduces XCS software and digital design.
    - Design constraints can be introduced to limit learners to decorating a templated square faceplate with engraved details (simplest option), or kept more open-ended for learners to fully custom the shapes of their face plates (more in-depth).
      - Design and format engraved details.
      - Design and format wooden face plates.
      - Design and format interior cardboard layers.
- Designs can be **hand-drawn** and digitally optimized, or designed **entirely digitally** based on your preference.
- **Curricular tie-ins**: art, design, biology, natural history, environmental science
  - Picture book tie-in: *Hector the Collector*
  - Create an herbarium.

### 2. Exploration: Add 3D-Print Jumbo Wing Nuts
Fabricate your press as normal, and then design 3D-printed, press-fit caps to upgrade off-the-shelf nuts into jumbo wing nuts.

- Introduces design for 3D printing (modeling a real-world object; using one model to "cut" another).
- **Curricular tie-ins**: art, design, biology, natural history, engineering, environmental science
  - Discuss creative problem-solving

### 3. Deep Dive: Add Vinyl Cut "Specimen Jars"
Design and vinyl cut clear "specimen jars," ornaments, or other artistic creations to show off pressed botanicals.

- Introduces vinyl cutting.
- Can make ornaments, bookmarks, jewelry, suncatchers, greeting cards, and more.
- **Curricular tie-ins**: art, design, biology, natural history, environmental science
  - Picture book tie-in: *Hector the Collector*
  - Create an herbarium.

### Extension: Build an Herbarium
  - A multi-week or season-long unit that asks learners to collect various stages and varieties of botanicals in their localities and stage them for presentation.
    - Specimens should be labeled, with short descriptions of their characteristics, where they were found, and when.
    - Learners should identify whether specimens are native, non-native, or invasive species and research their traditional, historical, and/or cultural uses (if any).

{:toc}

## Materials
- (1/3) sheet of 3mm basswood, plywood, or acrylic - enough to accommodate two 5” x 5” cutouts (face plates)
- (1) sheet of corrugated cardboard - enough to accommodate six 4” x 4” cutouts (inner layers)
- (4) 35mm bolts
- (4-8) washers
- (4) wing nuts (or regular nuts w/ 3D printed wing nut caps)
- (10) sheets of paper for inner layers (can be newspaper / 2x inner layers + 2)
- Extensions:
  - Vinyl cut preservations jars: clear vinyl (must be clear!)
  - Winged nut caps: 3D printer filament in a color of your choice
- Optional modifications: instead of bolts and wing nuts, you can use velcro straps or rubber bands to compress the two outer faces of the press.


{:toc}


## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas:
    -

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Explore the integration of art and science.
2. Exercise design thinking to brainstorm, plan, and execute a design that reflects their personal interests or aesthetic preferences.
3. Understand the scientific basis and historical context of botanical preservation.
4. Define and understand the importance of invasive versus native species.
5. Explain ecological concepts / demonstrate an understanding of plants’ roles in our ecosystem and their significance to environmental science / our food chain.
6. Translate a concept into a digital design suitable for fabrication technologies.


### Fabrication goals
Learners will be able to…
1. Translate an idea into 2D design for engraving.
2. Translate a 2D design into a digital file.
3. Prepare a file for the laser cutter.
4. Use the laser cutter to cut and engrave.
5. (Optional) Design in 3D and 3D print a file.
6. (Optional) Format files for vinyl cutting & use the vinyl cutter.



## Facilitator Notes
### 1. Setup & pre-preparation
- Learners can work alone or in groups of 2-3.
- Pre-sort materials (nuts, bolts, washers) into kits for each learner or table.
  - - For one-off workshops or younger learners, consider pre-printing jumbo wing nut caps and adding them to the kits. They are easier for little fingers to manipulate.
- If learners will be hand-drawing their decorative designs, print or design a worksheet for each of them. Ask them to constrain their designs to the dedicated template boxes provided. These show the real size of the finished face plates and the placement of the bolt holes.

### 2. Batch processing
- Designs can be cut multiple at a time (3 learners' sets / 6 frames)instead of one by one.

### 3. Time-savers
  - If constraining all learners to the same size/shape face plates, you can **pre-cut and distribute the cardboard inner layers** and have them design and cut only the plates themselves.
  - If constraining the size and shape of the face plates, you can also **pre-cut and distribute the bottom plates** and have learners customize only their top plates.
    - This also allows for easy branding of bottom plates if in a workshop setting.

### 4. Adjustments
- For young learners or those with disabilities affecting their fine motor skills, consider swapping out the nuts and bolts for rubber bands or velcro straps.
  - Indents can be laser cut into the sides of the face plates to keep these aligned.  

### 5. Material considerations
- Acrylic can also be used for face plates if desired.
- Inner layers should be corrugated (compressible) cardboard.

### 6. Design considerations to share with learners
- Remember you will need a minimum of three holes placed around the perimeter of your face plates, roughly equidistant from one another in order to provide even pressure when the press is screwed shut.
  - Add these first so you don't risk interfering with your engraving designs later.

### 7. Best practices for collecting botanicals
- Follow the one-in-twenty rule: if there are 20 of a specific flower in bloom, it’s generally okay to take one—if it’s a species that’s not endangered or protected.
- Science tip: Keep your press somewhere cool to speed up drying and help preserve color.

## Extensions
1. **Make an herbarium**: Natural history museums catalog different species of flora in collections called herbariums. To create your own herbarium of local native or invasive flora, mount dried flora to cardboard or sheets of cardstock. Traditionally, these would have been sewn in place.
  - Arrange specimens as naturally as possible (in a way similar to how they might be found in nature)
  - Show both sides of the leaves; characteristic coloring or markings might only be identifiable on one side. (For things with fronds, bend one backward.)
  - Add minimalistic identifying information: the plant’s scientific name, the date it was collected, and where it was found.
2. **Montessori**: Take learners on a nature walk and ask them to identify and pick local flora. Give them a limit on the number they can choose, and emphasize the importance of leaving sensitive or endangered species untouched. (Review these together before setting out.)
3. **Science**: Can start the lesson with a dissection. Fold the front petals of a flower down before pressing it to expose the pistils and stamen hiding in its center. Once it’s dry, use glue or pins to mount it to cardboard and create an anatomical diagram labeling its parts.
4. **History & social studies**: Discuss how indigenous peoples used dried plants for medicinal purposes and to create colorful dyes.
5. **Art**: Glue your pressed botanicals down to make a collage-style self-portrait.


## Discussion questions
- **General**:
1. Why do scientists collect objects from nature?
2. What could we learn from a plant collection?
3. What information should botanists include on an herbarium label? Why?
4. If you were to start a natural history collection, what would you collect? Why?

- **Art & Design**:
1. How could/did you incorporate artistic elements into the design of your botanical press?
2. What are some creative ways to display your pressed botanicals once they are preserved?
3. How can pressed botanicals be used in different forms of artwork?

- **History & Social Studies**:
1. How has the practice of botanical preservation evolved over time?
2. In what ways did historical botanists contribute to our understanding of plant species?
3. How can preserving plants help in understanding the historical and cultural significance of different species?

- **Science (biology, environmental science)**:
1. What are the differences between native and invasive plant species, and why is it important to understand these differences?
2. How does pressing flowers and leaves help in studying plant biology?
3. What role do plants play in our ecosystem, and why is it important to preserve them?

- **Creativity & Problem Solving**:
1. What challenges did you encounter during the design and fabrication process, and how did you overcome them?
2. What creative techniques or tools did you use to when designing your press, or when using specimens from it?
