---
layout: default
title: 2. Paper Pop-Ups Activity Steps
nav_order: 7
mathjax: true
---


# Paper Pop-Ups: Cut, Crease, and Fold a Custom Name Tag
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Use a vinyl cutter to make two-dimensional artwork “pop!”

Add score and cut lines to cardstock to fabricate dynamic, custom name tags that can fold flat when not in use.

This activity introduces the vinyl cutter and Silhouette Studio software at a beginner level.

### Context

Paper pop-ups are a form of engineering that use basic forces—simple pushes and pulls—to fold and unfold dynamic three-dimensional scenes from simple cardstock.

Some of the earliest pop-ups were used to make books and other texts interactive, inviting readers to lift flaps, pull tabs, and unfold elaborate spreads tucked into the pages.

Today, pop-up paper engineering remains common in decorative items like ornaments and greeting cards. Many designs draw inspiration from the Japanese twin arts of origami and kirigami.

While origami relies on folds and creases alone, kirigami incorporates cuts as well.

{:toc}

## Materials
- Silhouette Studio software
- Vinyl cutter
- Light hold (low-adhesion)cutting mat
- Cardstock (preferably 100lb or higher)
- Decorative craft materials (googly eyes, feathers, sequins, etc.)



## Activity Steps


### 1. Prepare a New Project in Silhouette Studio
First, you need to set up your digital workspace.
1. Open a new project in Silhouette Studio (downloadable here).
2. Input the size of the material you will be working with.
  - Under “page setup,” either select from the dropdown menu under “media size” or manually input the width and height in the text boxes below.
  - The other settings should be fine as-is, but feel free to adjust the media color or rotate the view as desired.

### 2. Create your design!
You can either work directly in Silhouette Studio, import an existing .png or .svg file from the web (respecting copyright restrictions), or bring over a design from a third-party design software like InkScape.
- **To add text**: Choose the “text” tool on the lefthand menu, use your cursor to select your placement, and type your desired text. A toolbar will appear across the top of the screen where you can select your desired font style and size. Bolder, blockier fonts work best for pop-ups.
- **To import a file**: Drag and drop!

### 3. Add a bounding box
This will be the outer shape of your finished card or name tag.
- Create a bounding box in the shape of your choice, sized to how large you want your finished piece.
- Center this around your pop-up design.

### 4. Add & format pop-up details
Silhouette Studio has a built-in pop-up tool that will automatically add many of the elements you need. This is somewhat limited, and works best to create supports for other decorative elements (for example, if you wanted to glue colored letters in place on top of pop-up scaffolding.) However, we can make it work for these name tags, too.

##### 4.1 Open the pop-up design panel
- In the righthand menu, click on the pop-up icon. (This looks like a tiny card with a heart in it.)

##### 4.2 Toggle the switch to "on"
- Some new solid and dotted lines will appear on your design.
  - Dotted lines indicate creases.
  - Solid lines indicate cuts.

##### 4.3 Place & adjust centerline
- The horizontal dotted line illustrates where the **centerline crease** in your card will fall. Drag the tiny double-pointed arrow icon up or down to adjust its placement.
  - This doesn’t affect the height of the design itself; just how far out it will “pop.”
- Drag the red dots to adjust the **width** of the center crease.
  - Make sure the dotted centerline extends the full width of the bounding box so your finished card will fold all the way across.

##### 4.4 Adjust pop-out depth
- Adjust the height of your design by clicking and dragging the white registration mark at the *top*.
- Adjust how far out your design "pops" by clicking and dragging the white registration mark at the *bottom*.

##### 4.5 Adjust cut and crease lines
- **Base width**: How much material connects the bottom of the design to the page. If this exceeds the width of the legs on any of your letters, they won’t cut properly.
- **Strut width**: How many supporting connections connect the top of your design to the back of the card, and their thickness.
- **Dash pitch**: The width of the perforation cuts for your creases.

### 5. Configure cut settings

Now, it's time to prepare the file for the printer.

1. Navigate to “send” in the main menu (upper right corner of the screen).
2. Select “line” from the top menu. Each line color—-in this case, red and blue--should have its own row.
3. Choose your material for both rows (they will be the same; likely cardstock, plain although coverstock, heavy also works, depending on your material).
4. Select an action for each row: cut for red and score for blue.
 - If you are using the autoblade tool, you can perform both functions in the same pass. Choose tool no. 1 for both rows. (*Note: your scored lines will be finely perforated rather than scored.*)
 - If you are using two different tools—one for cutting and another for scoring—choose the correct tool number and carriage as appropriate for each row.


### 6. Set up the vinyl cutter
Be sure to set up somewhere with enough room for the mat to feed through. There's no need for a spool feeder for this project, since you'll be using cardstock and a mat.
1. Turn the machine on: long-press the power button on its right side for 2-3 seconds.
2. Connect to the machine via bluetooth or a USB cable.
3. Select the appropriate machine from the menu once it appears.
  - (Note: if a firmware update is required, you will need to install this before proceeding.)
4. Install autoblade (skip this step if already installed):
  - Open the machine’s hood.
  - On the tool carriage, pull the locking mechanism completely out.
  - Place the autoblade into the tool slot, and make sure it is fully inserted.
  - Push the locking mechanism back into place.


### 7. Prepare your material on a mat
We'll be using a **low-adhesion mat** for this project.
1. Position your cardstock to align the upper left corner of the material with the upper left corner of the mat and press it firmly into place.
2. Line the mat up with the machine.
3. Push the arrow button to load the mat, centering it between the pressure wheels.
4. Use the arrow buttons to align the toolhead with the upper right corner of where you wish to cut.

### 8. Cut!
Almost done!
1. With your mat lined up and ready, click "send file."
  - The machine will take a few moments to process your file, cutting first and then scoring.
2. When the machine has finished, press the unload arrow to release the cutting mat.

### 9. Remove your material from the mat.
To remove your workpiece, peel the mat off of the cardstock instead of vice versa. This helps keep your finished product flat and intact. Peeling the workpiece from the mat will likely result in torn or curled cardstock.
  - Turn the mat over so your workpiece is flat on the table. Then, gently peel the mat off of the workpiece.

### 10. Bend and crease
Bring your design into three dimensions.
1. Gently bend along the mountain and valley folds to create folds.
  - Once you are happy with these, you can go back through with more force, using your fingernails or a 3D-printed tool—to create creases.
2. With your creases in place, carefully fold the whole card flat.
  - Be patient; it’s easy to accidentally bend a crease backwards the first time.
3. Glue the workpiece to a backer if desired, being careful to line up their center creases.
4. Decorate:
  - Add doodles, sequins, googly eyes, or whatever other decorative elements tickle your fancy!
