---
layout: default
title: 8. Custom Dice Activity Steps
nav_order: 20
mathjax: true
---


# 3D Print Custom (or Weighted!) Dice
By Claire Dorsett
{: .no_toc}

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Design your own 3D-printable dice!

Use CAD (computer-aided design) software to build out an idea in three dimensions.

Choose your dice geometry and decide what to put on each face. Print a cubic, numbered design or a polyhedral, text-based one—or make your die weighted to skew the odds!


### Context
The earliest known dice, excavated from Mesopotamian tombs and dating back to around 3000 BCE, were made from materials like bone, wood, and stone. Historians speculate that these were used not only in games of chance, but also for decision-making. Later, in ancient Greece and Rome, dice games were popular pastimes.

The fairness and randomness of dice are crucial to maintaining the integrity of games of chance. A fair die is one that has an equal probability of landing on any of its faces. A standard six-sided die should have a 1 in 6 chance of landing on any outcome on any given roll. This requires significant precision in modern-day manufacturing processes to balance the weight and shape of the die. Any imperfections, such as asymmetrical faces, uneven weight distribution, or air bubbles within the material can bias the roll, making certain outcomes more likely than others.

To assess whether dice are truly fair and random, statistical tests and experiments are often conducted. These involve rolling the dice a large number of times and recording the results to determine whether each face appears with roughly equal frequency.

Beyond gaming, the principles of probability explored through dice have practical applications in fields like computer science, where random numbers must be generated for simulations and to encode messages.


{:toc}

## Materials
- Computer with CAD software
- 3D printer
- PLA filament in a color of your choice


## Activity Steps

### 1. Ideate
#### 1.1 Choose a geometry
Like other polyhedra, dice have three features:
1. **Faces**: the sides of a three-dimensional body.
2. **Edges**: Straight runs where two sides meet.
3. **Vertices**: Points, where multiple sides meet.

A standard cubic has six faces, but polyhedral dice have more—sometimes many more. Dice used in games like Dungeons and Dragons, can have up to 20 faces! Do a little research to identify the name of your chosen geometry.

**Start by determining the number of sides your die will have.** Then work backwards to figure out its geometry. Some quick research will help you figure out its name.

Some things to note:

  - On *regular* polyhedra, all faces are equivalent to each other, as are all
  - On *mirror-symmetric* polyhedra, two identical, many-sided pyramids appear joined at their bases.

There are many other geometries, too. These are just the most common!


#### 1.2 Select a theme
Dice can sport more than just numbers. Their faces can feature words or symbols instead!  

1. Choose a theme for your die. Will its faces feature numbers? Colors? Phrases? Here are some ideas to help you get started:
  - **Story starters**: Dedicate multiple dice to different story components (genres, settings, characters, conflicts, heroes, villains) or parts of speech (nouns, verbs, adjectives, adverbs). Then roll the dice and build a story using the results.
  - **Decision makers**: Take inspiration from the Magic 8-Ball! Write a different decision word on each face and let the dice decide for you: yes, no, maybe, roll again.
  - **Mindfulness aids**: Describe a different breathing exercise or calming technique on each face.

2. List out the specific features. Make sure you have one per side. (It can help to start with a numbered list.)

3. Map out which features will appear where (on adjacent faces versus opposite ones, for example). Sketching the die in three dimensions from a few different perspectives may help you visualize this.

### 2. Design
You can use any CAD or 3D design software you like for these steps. While specifics will depend on which software you choose, the steps outlined below offer a high-level overview of the design process for a traditional **cubic** (six-sided) die.

#### 2.1 Model your die's geometry
1. Start with a square. Use the sketch function to lay this out in two dimensions.
  - The dimensions of each side should be equal to your desired dimensions for the finished object. (*Standard dice are 16mm or .66 inches cubed, but you can also go for an oversized option like 25mm or 1 inch.*)
2. Extrude your square into a cube. Select your square and use the extrude command, entering your desired side dimension into the distance box.
  - To “extrude” something means to pull or push it in a set direction. (When you squeeze a tube of toothpaste, you’re extruding it through the hole in the tube.)
  - In CAD, you can extrude (a) enclosed, two-dimensional shapes or (b) faces of a three-dimensional object.
3. If you’d like to round the corners or edges of your dice, you can use the “fillet” command. Fillets replace an angle with a curved arc. You can determine the curve by selecting its radius. Play around with this until you get a shape you like. Apply it to all edges or corners.

#### 2.2 Add design features to your faces
You will design each face individually now that your body is done.
1. Click the first face and choose “create sketch.”
You will now be able to design in two dimensions directly on the face you’ve selected.
2. Use the CAD tools to add or design your text, numbers, or symbols as desired.
3. Repeat this process until you have designed all six sides of your dice.

#### 2.3 Extrude design features
Since you’ll be 3D printing these dice, its features must be three dimensional. That means we need to extrude again.

You have a choice to make here. Your features can either be 1) proud, meaning they are raised off the dice’s surface, or 2) recessed, meaning they will look engraved or cut into its surface.

Keep in mind that raised features might affect how your dice rolls or bounces off a tabletop!

1. Select your designs (hold command to select them all at once!).
2. Choose the extrude command again.
3. Under "distance,"" enter either a positive or negative number (we recommend 2-3mm, or about 0.08inches).
  - A positive value will create raised features; a negative one will create recessed features.
4. Choose “join” for a positive number or “cut” for a negative number.

#### 2.4 Save your file
1. Save your finished dice as a .stl file.
  - This is the file format most commonly used for 3D printing. Often referred to as a “mesh,” it is an intricate three-dimensional web made up of thousands upon thousands of tiny triangles. (*STL stands for stereolithography, but you can think of it as “standard triangle language” or “standard tesslelation language” instead.*)

### 3. Prepare and slice files
Slicing softwares, often called “slicers,” are used to prepare .stl files for 3D printing. They offer tools and workflows to help you lay out multiple bodies on a single print bed, add supports, and more. You cannot skip this step!

1. Open your slicing software: Bambu Studio
2. Import your file into the software. (Use the file icon, or simply drag and drop.)
3. Select the type of printer you’re using (P1S).
4. Select the filament type being used: likely PLA.
5. Select the slicing settings.
  - You may need to add supports here.
6. Click “slice.” This will create a .3mf file and take you to a preview window that shows you what your finished design looks.

Your die is now ready to print!

### 4. Launch Print
  You have two options to launch your print: 1) send it wirelessly, or 2) us an SD card.
1. Load filament (skip this step if already loaded):
    - Hang filament spool on holder on back of machine so that filament unwinds clockwise.
2. Insert filament into the PTFE tupe on the top back side of the machine.
3. On the machine’s screen, select nozzle → – → bed heat, and head the nozzle to the recommended temperature for the filament (for PLA).
4. Wait for the nozzle to heat up.
5. On the machine’s screen, select nozzle → E → downward arrow several times until the filament comes out of the nozzle.
6. Load file: Select the file icon and select your file to start the print.
- Note: The printer will likely run an automatic leveling check before printing. This usually takes a few minutes.

### 5. Retrieve finished dice
Once the printer is done, pop your dice off the bed.

If they seem stuck, you can either use a soft prying tool (a 3D printed one works well!), or remove the magnetic bed and gently flex it to help the objects release.

Use pliers to remove any supports.

### 6. Give your dice a roll!
Give your finished die a roll! Does seem to land randomly, or to favor one result over the others? How might you adjust your design to change this?
