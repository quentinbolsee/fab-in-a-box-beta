---
layout: default
title: Vinyl Cutter Tutorials
nav_order: 3
mathjax: true
---

# General Tutorials
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}


## Vinyl Cutting
### Silhouette Cameo 5 Vinyl Cutter

#### Setting up the vinyl cutter
Be sure to set up with enough room for the spool holder if you're using it. If you're using sheet vinyl, there's no need for a cutting mat.
1. Turn the machine on: long-press the power button on its right side for 2-3 seconds.
2. Install autoblade (skip this step if already installed):
  - Open the machine’s hood.
  - On the tool carriage, pull the locking mechanism completely out.
  - Place the autoblade into the tool slot, and make sure it is fully inserted.
  - Push the locking mechanism back into place.

#### Connecting the vinyl cutter
1. With the machine on, connect it to your computer via bluetooth or a USB cable.
2. Select the appropriate machine from the menu once it appears.
  - (Note: if a firmware update is required, you will need to install this before proceeding.)

#### Loading material (loose, matted, or from spool holder)

**If you are using loose sheets (vinyl) or a matted material (cardstock, etc.)**:
1. Press the forward arrow button while feeding one end of the material into the machine, **making sure it is aligned under both rollers**.

**If working from a roll, using the spool holder**:
1. Set the spool holder several inches front of the machine.
2. Load the vinyl roll onto the spool holder, with its loose end on the top facing away from you and toward the machine.
3. Press the forward arrow button while feeding the end of the roll into the machine, **making sure it is aligned under both rollers**.
4. Use the arrow buttons to align the toolhead with the upper right corner of where you wish to cut.

#### Removing (unloading) material
Press the backward arrow button while gently pulling the end of your material back toward you. it is aligned under both rollers.

#### Using a mat
Different mats have different **adhesion strengths**. Low-adhesion mats are sufficient for most purposes, and are easier to remove material from.
1. Position your cardstock to align the upper left corner of the material with the upper left corner of the mat and press it firmly into place.
2. Line the mat up with the machine.
3. Push the arrow button to load the mat, centering it between the pressure wheels.
4. Use the arrow buttons to align the toolhead with the upper right corner of where you wish to cut.


#### Removing material from a mat
To remove your workpiece, peel the mat off of the cardstock instead of vice versa. This helps keep your finished product flat and intact. Peeling the workpiece from the mat will likely result in torn or curled cardstock.
  - Turn the mat over so your workpiece is flat on the table. Then, gently peel the mat off of the workpiece.


#### Running a job
1. With your material lined up and ready, click "send file."
  - The machine will take a few moments to process the job.
2. When the machine has finished, press the unload arrow to release the material while slowly pulling it out of the machine.
  - Note: Vinyl cutters only cut; they don’t remove unneeded material. That means your finished pieces may *look* like solid vinyl at first glance. Hold it at an angle to the light to see the cut lines more easily.





### Silhouette Studio Software Tutorials
#### Downloading Silhouette Studio
Download Silhouette Studio software for free from their website here:

#### Selecting materials
1. Click "send" in the upper right menu.
  - The "simple" tab is fine if all of your lines will be processed the same way.
  - If you have multiple kinds of processing (e.g. creasing and cutting), choose the "line" tab and assign a different action type to each line color.
2. Select your material from the dropdown menu.
3. Assign an action to the material.


#### Designing
- Silhouette Studio allows you to use a combination of text, shape tools, and vectors to translate your idea into the digital world. These can be found in the lefthand toolbar.
- You may often want to focus on composition before sizing. You can resize everything at once later.

#### Importing an image
Silhouette studio accepts both .png and .svg files.
1. Click the folder icon in the upper lefthand corner.
2. Select your desired file.
*-or-*
1. Drag and drop a file from your desktop onto the canvas.

#### Formatting for cutting or scoring:
Now, it's time to prepare the file for the printer.
1. Navigate to “send” in the main menu (upper right corner of the screen). It should automatically open to "simple."
3. Choose your material from the dropdown menu.
4. Select an action: cut.

#### Useful tools and their applications
- **Cursor**: Allows for selection of images, dragging and dropping, etc.
- **Edit points**: Allows you to drag-and-drop and/or otherwise manipulate the points in a polygon or curved shape. (For curved shapes, allows you to adjust the bezier curve.)
- **Line tools**: Lots of ways to draw!
  - **Draw a line**: Straight lines only. Hold shift to draw a perfectly vertical or horizontal line, or to snap to a 45-degree diagonal.
  - **Draw a polygon**: Allows you to drop multiple points in whatever arrangement you choose, drawing *straight* lines between them. Closes the shape into a vector when you click back on the starting point. (Double click to end the shape where it is / leave it open.)
  - **Draw a curve shape**: Allows you to drop multiple points in whatever arrangement you choose, drawing *curved* lines between them. Closes the shape into a vector when you click back on the starting point. (Double click to end the shape where it is / leave it open.)
  - **Draw an arc**: Creates smooth arcs with the angle measurements shown. (Snaps to common angles.) Double point arrows allow the manipulating of the arc's size once it is drawn, while the red points allow you to change its angle.
- **Shape tools**: A (limited) library of shapes.
  - **Rectangle**: Draws rectangles. Hold shift to create a perfect square.
  - **Rounded rectangle**: Draws rounded rectangles. The rounding of their corners is adjustable by manipulating the red points. Hold shift to move them the same distance, together.
  - **Ellipse**: Draws ellipses. Hold shift to create a perfect circle.
  - **Regular polygon**: Input the number of sides you want to create pentagons, hexagons, etc.
- **Drawing tools**:
  - **Draw freehand**: Creates a continuous line following your cursor including sharp corners.
  - **Draw smooth freehand**: Creates a continuous line following your cursor, rounding sharp corners to create smooth transitions.
- **Text**: Input text and adjust its font size and style.
- **Draw a Note**: Adds a sticky note to allow you to jot down notes to yourself.
- **Eraser tool**: Erases lines.
- **Knife tool**: Cuts.

#### Grouping & ungrouping objects
- Once you’re happy with a design's composition, you can group all objects together and manipulate them as a single unit.
  - Select all objects > right click > group.
- Follow the same process to ungroup objects if you need to manipulate them independently again.

#### Resizing an object
- Select the object and drag the points that appear around it to resize.
  - Manipulating corner points will maintain an object's height-to-width ration.
  - Top or side points will adjust only the object's height or width (respectively).

#### Adding an outline around an object
1. Select the object.
2. Right click.
3. Select "offset"
4. In the pop-up menu (upper right corner), click "offset" or "internal offset"
  - offset = creates a larger outline around the outside of your original object
  - internal offset = creates a smaller outline inside your original object.
5. Input a value into the "distance" box.
6. (Optional) Choose a corner style (angled or rounded).

#### Reflecting an object
1. Select the object.
2. Right click.
3. Select "flip horizontally" or "flip vertically"

#### Adding text
Choose the “text” tool on the lefthand menu, use your cursor to select your placement, and type your desired text. A toolbar will appear across the top of the screen where you can select your desired font style and size.



### Useful Knowledge

#### Weeding
This is many people's least favorite step, as it requires a lot of patience.
1. Under good lighting, use picks, tweezers, or your fingernails to carefully peel away all vinyl that is **not** part of your finished design. This can be discarded.
  - It can help to gently hold pieces you want to keep in place as you remove elements beside them; sometimes, two or more sections will come off together. This is particularly true for tiny details, like punctuation marks.

#### Using transfer tape or transfer paper
Once your design has been weeded, it’s ready to be transferred to its new home!
1. Cut a piece of transfer tape or paper to a size appropriate for your design.
2. Peel the bottom (opaque) layer off of the tape and apply the sticky layer carefully over your design, avoiding any wrinkles or bubbles.
  - A plastic card (like a hotel key or student ID) can be a useful tool to help press the transfer tape down, making sure it sticks firmly to the vinyl.
  - Note: don’t remove the tape until you’re ready to stick your design to its final resting place! If you need to transport it, do so with the transfer tape still affixed to it.
3. Once you’re ready to apply your sticker to a surface, carefully remove the back layer from the vinyl. You will be left with just the vinyl itself, stuck to the transfer tape.
4. Line up your sticker where you want it, and gently press it into place. Like before, it can help to gently scrape over it with a plastic card to make sure adhesion is strong.
5. Gently peel off the transfer tape, and admire your new sticker!

#### Creating pop-ups
Silhouette Studio has a built-in tool to help you automatically create pop-ups from text or shapes. This is somewhat limited, and works best to create supports for other decorative elements (for example, if you wanted to glue colored letters in place on top of pop-up scaffolding.) However, we can make it work for just a single layer, too.

##### Create your design
Pop-ups work best with simple, blocky shapes or bold, blocky text (with capital letters only).

##### Add a bounding box
This will be the outer shape of your finished pop-up (the thing that your design pops out of).
- Create a bounding box in the shape of your choice, sized to how large you want your finished piece.
- Center this around your pop-up design.

##### Open the pop-up design panel
- In the righthand menu, click on the pop-up icon. (This looks like a tiny card with a heart in it.)

##### Toggle the switch to "on"
- Some new solid and dotted lines will appear on your design.
  - Dotted lines indicate creases.
  - Solid lines indicate cuts.

##### Place & adjust centerline
- The horizontal dotted line illustrates where the **centerline crease** in your card will fall. Drag the tiny double-pointed arrow icon up or down to adjust its placement.
  - This doesn’t affect the height of the design itself; just how far out it will “pop.”
- Drag the red dots to adjust the **width** of the center crease.
  - Make sure the dotted centerline extends the full width of the bounding box so your finished card will fold all the way across.

##### Adjust pop-out depth
- Adjust the height of your design by clicking and dragging the white registration mark at the *top*.
- Adjust how far out your design "pops" by clicking and dragging the white registration mark at the *bottom*.

##### Adjust cut and crease lines
- **Base width**: How much material connects the bottom of the design to the page. If this exceeds the width of the legs on any of your letters, they won’t cut properly.
- **Strut width**: How many supporting connections connect the top of your design to the back of the card, and their thickness.
- **Dash pitch**: The width of the perforation cuts for your creases.
