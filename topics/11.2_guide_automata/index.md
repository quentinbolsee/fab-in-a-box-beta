---
layout: default
title: 11. Automata Facilitator Guide
nav_order: 27
mathjax: true
---


# Faciltator Guide: Fabricate a Custom Automaton
By Claire Dorsett, with inspiration from The Exploratorium's "Cardboard Automata" activity

## Table of contents
{: .no_toc .text-delta }

- TOC
{:toc}


## Overview
Learners will design and fabricate a custom automaton, exploring the intersection of art and science.

They will assemble a laser- or vinyl-cut box frame and add cams and followers to engineer a desired movement. Then, they will design and fabricate a topper that harnesses that movement to tell a story or illustrate a scene.

This activity can be done entirely on the laser cutter or with the vinyl cutter and 3D printer thrown in.  


### Context
From the Greek word automaton, which means “self-moving,” automata are kinetic sculptures that exhibit movement through mechanical means. They lie at the intersection of art and engineering, requiring both creative vision and a working knowledge of mechanical principles to construct.

Historically, automata were considered almost magic, moving in highly complex ways driven by—sometimes hidden!—hand-cranked gears and complex systems of simple machines.

Today, technology enables remote control through motor-powered systems.

{:toc}

## Scalability
### 1. Exposure: Design a Topper that Flaps
Assemble pre-supplied components (press-fit box frame, crank, driveshaft, cams, and followers) into an automaton designed to transform circular motion into linear motion (i.e. it move a post up and down when the handle is cranked). Then, design and fabricate a custom winged topper that "flaps."

- Light lift: most components are pre-supplied (we recommend pre-assembling frames).
- Teaches assembly and vinyl cutting (wings).
- Can be done in 2D or 3D design software, or crafted through analog means.
- **Curricular tie-ins**: engineering (cranks, cams, followers), science (simple machines, energy & motion), math (sine waves in motion), english language arts (storytelling)
  - Discuss translation of rotational to linear motion


### 2. Exploration: Design and Fabricate a Cam/Gear System (+ Topper)
Engineer your own motion system by designing cams and/or gears that can be threaded onto a pre-supplied driveshaft. Then design and fabricate a custom topper that takes advantage of the motion you've created (circular, back and forth, etc.).

- Allows for greater customization without designing from scratch.
- Uses pre-supplied files / parts for the box frame and driveshaft (learners can assemble these themselves for this version), freeing them up to focus on designing custom cam and gear systems.
- Can be done in 2D or 3D design software, or crafted through analog means.
- Invites learners to create more complex motion systems.
- **Curricular tie-ins**: engineering (cranks, cams, followers), science (simple machines, energy & motion), math (sine waves in motion), english language arts (storytelling)
  - Discuss multiple motion systems (can have different learners each make a different one for the sake of comparison before starting their own designs).
  - Can have learners prototype in analog with cardboard or craft foam before translating to digital design, if appropriate.


### 3. Deep Dive: Start from Scratch
Use CAD software to engineer an automaton from scratch: design a press-fit box frame, driveshaft, cams or gears, followers, and a topper (which can also be crafted through analog means).

- Introduces more complex spatial reasoning and design in three dimensions.
- Open-ended customization.
- Recommended for multi-session instruction, including teaching caliper use, press-fit assembly, motion systems, etc.
- **Curricular tie-ins**: engineering (cranks, cams, followers), science (simple machines, energy & motion), math (sine waves in motion), english language arts (storytelling)
  - Teaches CAD and whole-system thinking (possible to prototype in analog with plastic cups or tissue boxes, wooden skewers, and cardboard or craft foam before translating to the digital world).


### Extension: Add Motorized Movement
Challenge learners to add microservos to automate their driveshafts or add other mechanical motions.
  - They can also add vinyl-cut circuits (or copper tape) along with button batterys and LEDs to light up their creations.
  - Advanced learners can explore inputs and outputs, adding sensors that trigger movements based on their readings.



{:toc}

## Materials
- Laser cutter
- 3-5mm plywood or sturdy cardboard (box frames, driveshaft, follower shaft)
- Sturdy cardboard (cams & followers)
- Vinyl cutter
- Thick (~110lb) cardstock for vinyl cutter (wings)
  - Note: construction paper is not advised; it is too fibrous to cut cleanly.
- (Optional) 3D Printer
- (Optional) PLA filament in a color of your choice
- (Optional) Decorative craft materials for toppers (googly eyes, feathers, sequins, etc.)

{:toc}


## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Identify the basic mechanisms of movement in automata, including cams, cranks, gears, and driveshafts.
2. Plan, test, and adjust different cam-and-gear configurations to engineer desired movements.
3. Apply creative thinking and storytelling techniques to design and construct dynamic scenes that harness movement to convey a narrative or theme.
4. Articulate the design choices, mechanical principles employed, and storytelling elements in their creations.



### Fabrication goals
Learners will be able to…
1. Translate an idea into a digital design to be fabricated.
2. Choose between fabrication methods for their desired outcomes.
3. Select materials appropriate for different fabrication methods and desired performance outcomes (rigidity, opacity, etc.).
4. Apply problem-solving skills to troubleshoot and resolve fabrication challenges such as material distortion, machine errors, or design discrepancies.



## Facilitator Notes
### 1. Setup & pre-preparation
- Students can work alone or in groups of 2-3.
- Several examples should be provided to show different types of movement.
  - Many great examples are available online, but seeing the mechanisms in person can be especially helpful to understand how they fit together.
  - One example of rotational-to-reciprocal movement (turn the crank to make the topper rise and fall) -plus- one example of horizontal rotational to vertical rotational movement (turn the crank to make the topper spin in a circle) should be sufficient in most contexts.
- Offer examples of different shapes and combinations of cams, and the types of motion they create.
- If time is limited, box frames can be pre-assembled to free learners up to focus on the conceptualization, design, and fabrication of their toppers and/or cam systems.
- For workshop-style facilitation, this activity can be presented as a kit, with each learner or group given: press-fit parts for a box frame, a driveshaft, an assortment of cams, a 3D printed crank, and followers.
  - They would then have everything needed to experiment with designs, freeing them up to focus on crafting creative toppers.



### 2. Adjustments
- If time is limited, you can pre-fabricate (and even pre-assemble) frames and driveshafts. This frees learners up to focus more on cam shapes, gear interactions, and the scenes they want to illustrate with their toppers.
- All elements of this activity can be fabricated on the laser cutter alone; however, the vinyl cutter works well for cardstock wings and cranks can be 3D printed if desired. All machines can be used to fabricate the toppers, as time allows.
  - Connectors can also be 3D printed (between the crank and driveshaft; between the follower shaft and topper elements, etc.)


### 3. Batch processing
- If all learners are ready at the same time, cams (or sets of cams) can all be laser cut at once and wings can all be vinyl cut at once.


### 4. Design considerations to share with learners
- How can you combine mechanisms to achieve multiple movements, either in unison or in sequence?
- If laser-cutting, it is okay that driveshafts are square in cross-section as long as cams are cut with square holes that thread onto them (a round hole will spin freely on a round shaft if a perfect press-fit is not achieved).

## Lesson Plan Outline
Multi-session facilitation: 3 sessions (approximately 2 hours each)
### Session 1: Introduction to Automata and Frame Design
1. Introduction (15 minutes):
  - Introduce the concept of automata and show examples of different types of kinetic sculptures.
  - Discuss the basic components of automata, including frames, cams, gears, driveshafts, and followers.
  - Explain the project goal: to design and build an automata that incorporates a dynamic scene.
2. Design Phase (30 minutes):
  - Instruct learners to brainstorm ideas for their automata scene, considering themes, characters, and actions.
  - Guide learners in designing the frame for their automata using digital design software. Emphasize the importance of sturdy construction and precise measurements.
  - Demonstrate how to prepare the design files for laser cutting, including proper file formats and settings.
3. Prototyping (45 minutes):
  - Divide learners into small groups and provide them with access to the laser cutter and materials.
  - Instruct learners to laser cut their frame designs and assemble them.
  - Encourage learners to test their frame prototypes and make adjustments as needed to ensure smooth movement and stability.
### Session 2: Experimenting with Cams, Gears, and Driveshafts
1. Introduction to Mechanisms (15 minutes):
 - Review the concepts of cams, gears, and driveshafts, explaining how each component creates its respective movement(s).
 - Show examples of different cam and gear shapes, sizes, and types, and discuss their effects on movement.
 - Discuss the importance of experimentation and observation in understanding how mechanisms work.
2. Experimentation (45 minutes):
 - Provide learners with various cams, gears, driveshafts, and followers.
 - Instruct learners to experiment with different combinations of components to engineer desired movements.
 - Encourage learners to document their experiments and observations, noting the effects of different gear ratios, cam profiles, and follower placements.
3. Reflection (15 minutes):
 - Facilitate a discussion on the results of learners' experiments. Ask learners to share their findings and observations.
 - Guide learners in reflecting on how different combinations of cams, gears, and driveshafts can be used to achieve specific motions and effects.
### Session 3: Scene Construction and Final Assembly
1. Introduction (15 minutes):
 - Review the project goals and timeline for completion.
 - Remind learners of the importance of storytelling and creativity in designing their automata scenes.
2. Scene Design (45 minutes):
 - Instruct learners to finalize their scene designs, considering how they will integrate movement into their storytelling.
 - Provide learners with craft materials for constructing their scenes, including paper, cardboard, and miniature figures.
 - Encourage learners to think about composition, color, and detail in their scene construction.
3. Final Assembly (45 minutes):
 - Guide learners in integrating their scene designs with their frame prototypes and mechanisms.
 - Assist learners in attaching cams, gears, and driveshafts to their frames and ensuring proper alignment and functionality.
 - Encourage learners to test their completed automata scenes and make any necessary adjustments for smooth operation.
4. Presentation and Reflection (15 minutes):
 - Have each learner or group present their automata to the class, demonstrating how their scene comes to life through movement.
 - Facilitate a reflection discussion, allowing learners to share their experiences, challenges, and successes in designing and building their automata.
 - Encourage learners to reflect on how their understanding of mechanical principles and creative storytelling influenced their design process.



## Discussion questions
- **General**
1. How did you balance the artistic and mechanical aspects of your automaton design to create a cohesive and functional piece?
2. What creative techniques did you use to ensure that your topper integrated well with the mechanical movement provided by the cams and followers?
3. What criteria did you use to evaluate the success of your automaton design, both mechanically and artistically?
4. How are the principles of cams, gears, and followers used in real-world mechanical systems? Give four examples.
5. What are some historical or modern-day examples where you think automata may have been / may be used? (i.e Disney).
6. How could the addition of sensors and other electronic components enhance the functionality and interactivity of your automaton?

- **English Language Arts: Storytelling**
1. What storytelling elements did you incorporate into your automaton, and how did they enhance the overall design?
2. How does the movement of the automaton contribute to the narrative or theme you intended to convey?

- **Mechanical Design**
1. How does the shape of the cams influence the type of movement (e.g., linear, circular) produced in your automaton?
2. What considerations did you take into account when designing the cam profiles and follower shapes to achieve smooth (or intentionally jarring) motion?
3. How does the placement and orientation of cams and followers affect the efficiency and functionality of your automaton?
4. What challenges did you face in ensuring that the cams and followers stayed properly aligned during operation, and how did you address them?
5. If using gears, how do different gear ratios affect the speed and type of movement in your automaton?

- **Math**
1. How does your automaton's movement demonstrate math? (i.e. Sine waves)
2. Graph your design's motion.

- **Physics: Forces, Energy, Motion**
1. What types of energy transformations occur within your automaton as it operates (e.g., mechanical energy to kinetic energy)?
2. How do the principles of forces and motion in your automaton relate to real-world mechanical systems like engines and machinery?
3. What are some examples of energy transformations in everyday mechanical devices, and how do they compare to those in your automaton?
4. How does the addition of weight to the topper affect the energy required to operate your device?
5. How can you minimize energy lost to friction in your design?
