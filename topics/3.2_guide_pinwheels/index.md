---
layout: default
title: 3. Pinwheels Facilitator Guide
nav_order: 10
mathjax: true
---


# Faciltator Guide: Engineer a Paper Pinwheel
By Claire Dorsett
{: .no_toc}

## Table of Contents
{: .no_toc .text-delta }

- TOC
{:toc}

## Overview

Learners will design, vinyl cut, and fold custom paper pinwheels.

This activity can be run as an iterative or comparative design challenge, with learners experimenting (or competing!) to see which design can achieve the most rotations within a single minute.

A perfect complement for sustainable energy tie-ins, this lesson offers a tutorial in 2D design software and the vinyl cutter.

### Context

Paper

{:toc}



## Scalability
### 1. Exposure: Working from Pre-Designed Templates
Cut, crease, and fold a paper pinwheel.
  - Focuses on teaching the fundamentals of the vinyl cutter & digital software using pre-designed, four-bladed windmill templates.
  - Supports light customization (cardstock design, simple adjustments to existing templates).
  - **Curricular tie-ins**: engineering, design, sustainability
    - Explore how windmills/turbines catch the wind.
    - Discuss sustainable energy.

### 2. Exploration: Custom Designs & Decorations
Design and fabricate a paper windmill of your own design, experimenting with blade shape and number.
  - Focuses on teaching basic digital design principles in addition to the vinyl cutter itself, offering inspirational design ideas but not templates.
  - Can be run as an iterative design challenge or competition.
  - Can incorporate any number of blades as well as decorative or functional cutouts on the blades.
  - Facilitates (optional) exploration of optical illusions, including color mixing and spirals, etc as the blades turn quickly enough to blend these in unexpected ways.
  - **Curricular tie-ins**: engineering, design, spatial reasoning, sustainability
    - Explore how windmills/turbines catch the wind.
    - Discuss sustainable energy.

### 3. Deep Dive: Complex Geometries & Multiple Layers
Design & fabricate pinwheels with complex geometries and multiple layers capable of spinning in oposite directions.
  - Focuses on more advanced spatial reasoning and digital design principles.
  - Can be run as an iterative design challenge or competition.
  - Can incorporate any number of blades and decorative or functional cutouts, with multiple layers of stacked blades.
  - **Curricular tie-ins**: engineering, design, spatial reasoning sustainability
    - Explore how windmills/turbines catch the wind.
    - Discuss sustainable energy.

### Extension: Water Wheels
Swap cardstock for thin, waterproof plastic and challenge learners to create a water wheel instead of a wind-powered pinwheel.
- Explore how water flow can be converted into rotational motion, and how this can be translated into energy.
- Investigate design principles like blade shape, balance, and axle placement.
- Will likely require additional materials & assembly (learners will cut just the blade shapes, which they will then need to attach to a central axle.
  -  **Curricular tie-ins**: engineering, energy, sustainability
    - Expand discussions on sustainable energy.
    - Compare engineering considerations for wind- versus water-powered turbines.


## Materials
- Vinyl cutter
- Light hold (low-adhesion)cutting mat
- Cardstock (preferably 100lb or higher)
- Wooden pencils with rubber erasers
- Push pins
- Circular stickers (~1 inch diameter), tape, or glue
- Fan or blowdrier with "cool" option
- (Optional) Thin but rigid laser-safe plastic sheets


{:toc}


{:toc}


## Standards Alignment
- Next Generation Science Standards
  - Disciplinary Core Ideas:
    - Defining and Delimiting Engineering Problems (3-5-ETS1.A),
    - Developing Possible Solutions ((3-5-ETS1.B),
    - Optimizing the Design Solution (3-5-ETS1.C)
  - Science and Engineering Practice:
    - Planning and Carrying Out Investigations (3-5-ETS1-2);

- Common Core Standards
  -
- ISTE Standards
  -

## Learning Outcomes
### General goals
Learners will be able to…
1. Define and explain the relevance of radial symmetry as it relates to pinwheel design.
2. Understand and articulate the relevance of sustainable energy.
3. Describe how turbines translate rotational motion into electrical energy that can be stored.
4. Use spatial reasoning to design two-dimensional cut files that fold into functional, three-dimensional forms.
5. Think critically about how to engineer an efficient pinwheel blade shape.
6. Practice creative problem-solving skills by testing and refining their designs for optimal performance.


### Fabrication goals
Learners will be able to…
1. Create custom designs using graphic design software.
2. Prepare files for, set up, and safely operate the vinyl cutter.
3. Crease and fold vinyl-cut designs into three-dimensional forms.



## Facilitator Notes
### 1. Setup & pre-preparation
- (Optional) Print handouts for each learner or prepare a slide to demonstrate how basic, four-bladed paper pinwheels are folded.
- Create a set of **examples** to illustrate the possible design range for paper pinwheels (multi-layered, etc.).

### 2. Batch cutting
- If working within bounding boxes in xDesign, learners' designs can be batch cut to economize time and materials.
- If using the batch-cutting template (optimized for xDesign), each user account can be renamed so they match with a sticker model (01, 02, and so forth), so “Person 01” knows to work on “Sticker 01”, etc. All models are saved to a shared collab space.
- When finished, ask learners to color their models red so that you have a visual indication that they are done.

### 3. Design considerations to share with learners
- Pinwheels will experience friction at their pivot point: the axle (between the paper and pushpin/rod). Making the hole slightly bigger and/or loosening the pushpin slightly can help minimize this friction to allow pinwheels to spin more freely.
- Consider drawing on the pinwheels (even using the vinyl cutter, to introduce the pen holder) to experiment with color mixing or optical illusions.

### 4. Prototyping
- Quick, miniature paper prototyping can be useful for helping learners visualize how they can fold their blades and how different folds might affect the pinwheel's performance.
  - This can be done with printer paper, notebook paper, or construction paper and scissors.


## Discussion questions
- **General**:
1. How do you expect different blade shapes (e.g., straight, curved, angled) influence the performance of a pinwheel? Does this differ based on which direction you want it to rotate?
2. What are the advantages and disadvantages of using vinyl cutting for creating pinwheel components compared to scissors?
3. What considerations should/did you take into account when designing the number and size of the pinwheel blades?

- **Physics: Forces, Energy, & Motion**:
1. Where and how do pinwheels (and wind turbines) experience the four forces of aerodynamics (lift, drag, thrust, gravity)? How do these compete or interact to influence the rotation of your pinwheel?
2. How does friction at the pivot point (between the paper and the pushpin/rod) affect the efficiency of the pinwheel’s rotation?
3. What are some ways to minimize friction and ensure smooth rotation of the pinwheel?
4. How does the distribution of weight alnog and among the blades affect a pinwheel’s balance and stability?
5. What is the relationship between the wind speed and the number of rotations your pinwheel achieves in a given time?

- **Engineering**:
1. How many blades do real-world wind turbines typically have? Why do you think this is?
2. How might you design a pinwheel that can catch the wind in either direction? What would be the advantage, if any, to such a design?
3. What are the key factors to consider when designing a pinwheel? What about a water wheel?
4. How does/would the design of blades for water wheels differ from those for wind pinwheels? Which principles remain the same between the two?
5. How does changing the angle or curvature of the blades influence the pinwheel’s ability to capture wind and convert it into rotational motion?

- **Sustainability**:
1. How does the use of renewable energy sources, like wind power, contribute to sustainability?
2. What are some environmental benefits of using wind turbines or water wheels for energy generation?
